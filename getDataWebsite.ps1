# Will be implement

function sendPushBulletNotification($apiKey, $message) {
 
    # convert api key into PSCredential object
    $credentials = New-Object System.Management.Automation.PSCredential ($apiKey, (ConvertTo-SecureString $apiKey -AsPlainText -Force))
 
    # build the notification
    $notification = @{
        device_iden = $device_iden
        type        = "note"
        title       = "Alert from Powershell"
        body        = $message
    }
 
    # push the notification
    Invoke-RestMethod -Uri 'https://api.pushbullet.com/v2/pushes' -Body $notification -Method Post -Credential $credentials
}

function invokeHtmlData () {
    $url = 'https://www.ub.ro/stiinte/studenti'
    $HTML = Invoke-WebRequest -Uri $url

    $param = "Orar anul universitar 2022 - 2023"
    $param2 = "Studii universitare de licență semestrul II, anul universitar 2022-2023"

    $siteData = $HTML -match $param -and $param2
}
