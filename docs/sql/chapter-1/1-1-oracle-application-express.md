# 1-1 Oracle Application express

## 1. Objectives

- This lesson covers the following objects:
    - Distinguish between application software and system software and give an example of each.
    - Log-in to the Oracle Application express practice environment.
    - Execute a simple query to retrieve information from the Database.
    - Apply the rules of SQL to display all columns and a subset of columns specified by criteria.

## 2. Purpose

* Every day, in one way or another, we come in contact with computer applications.
* If you checked your email today, it was probably done using an application.
* If you bought an item at a grocery store, the clerk scanned the item using an application that calculated your bill and updated the store inventory.
* In this course, you will learn the syntax of SQL using the application called Oracle Application Express.

## 3. Application Programs

* Although computers have been around for a very long time (possibly before you were born), their use for business and personal computing didn't take place until application software programs were developed
* Application programs allowed the end `user-people` like you and `me-to` be able to byu fully developed, ready-to-use programs
* It was no longer necessary to know how the program worked, just that it did work and accomplished what we wanted it to do.
* Application program software is different from system software.
* System software consists of `low-level` programs designed to interact with the computer hardware.
* Operating systems, compilers and system utilities are examples of system software.
* In contrast, application software includes programs for word processing, database, gaming, email and graphics.
* Yahoo.com uses the Oracle database to store data.
* Rather than having everyone who wants to search the database or retrieve email learn SQL, an application has all of the SQL (and other coding languages) pre-programmed into it.
* With a few mouse clicks, users have access to all of the information they need.

## 4. Using Applications 

* An application is like a car. To drive a car, you need to know enough to make it work. It was friendly `shell` to hide all the things that you don't need to know, such as how the transmissions works or how fuel like petrol or diesel is used to power the engine.

* Could you ever get your driver's license if you had to demonstrate an understanding of every system - electrical, powertrain, hydraulic, fuel etc - used to make the car run?

## 5. Oracle Application Express

* In this course, you will use Oracle Application Express. This application enables many developers to build and access applications as if they were running in separate database.

* With build-in features such as design theres, navigational controls, from handlers and flexible reports, Oracle Application Express accelerates the application development process.

* Two components on Oracle Application Express are:

> - SQL workshop
> - Application Builder

* To learn SQL, you will use the SQL workshop component. To design an application, you use Application Builder.

* Oracle Application Express (Apex) is the tool that we will use to allow you to build tables and retrieve information from an Oracle database (or use DBeaver or any other tool and connect with the username and pass from teacher).

* When retrieving information from a database, you will often have to find a subset of the data based on specifics search criteria.

* Becoming familiar with SQL will help you more quickly find the information that you need.

## 6. Basic SELECT Statement

* The `SELECT *` command returns all the rows in a table

* The syntax is: 

```sql
SELECT * FROM <table_name>;
```
* Example:

```sql
SELECT * FROM employees;
```

## 7. SELECT Statement with a condition

* To return a subset of the data, modify the `SELECT` statement
> The syntax is: 
```
SELECT <column_name_1, column_name_2, etc> from <table_name> WHERE <condition>;
```

Example:

```sql
SELECT first_name, last_name, job_id FROM employees WHERE job_id = 'SA_REP';
```

## 8. Correcting errors

* When entering SQL commands, it is important to use the corrent spelling, otherwise you will get an error message.

For Example (SELECT: spelling incorrect):

```sql
SELCT * FROM employees;
```
- Would result in the error message:

<div align="center">

![error](../../assets/img/error.PNG)

</div>

* To rectify, simply correct the spelling and run again.

## 9. Correcting errors

* It is also important to use the correct names and spelling for columns and tables. For example (employees table name - spelling incorrect):

```sql
SELECT * FROM employee;
```

- Would result in the error message:

<div align="center">

![error-db-spell](../../assets/img/error-db-spell.PNG)

</div>

* To rectify, simply correct the spelling and run again.

* Another example could be from column name (`first_name column` - entered incorrectly):

```sql
SELECT name FROM employees;
```

- Would result in the error message: 

<div align="center">

![error-column-name](../../assets/img/error-column-name.PNG)

</div>

* To rectify, simply enter the correct column name and run again.

## 10. Terminology

* Key terms used in this lesson included:

- Application software.
- System software.
- Oracle Application Express.
- Syntax.
- Subset.
- Comparison Operator.

## 11. Summary

* In this lesson, you should have learned how to:
    * Distinguish between application software and system software and give an example of each.
    * Log-in to the Oracle Application Express practice environment.
    * Execute a simple query to retrieve information from the Database.
    * Apply the rules of `SQL` to display all columns and a subset of columns specified by criteria.