# 1.2 Relational Database Technology

## 1. Objectives

- This chapter will covert the following  objectives:
    - Define and give an example of a relational database.
    - Identify table-key terms, including row, column, field, primary key and foreign key.
    - Relate the importance of databases to everyday life.

## 2. Purpose

- Databases are part of you everyday lives even through most if the time we don't even think about then. If you have ever made an airline reservation, used an ATM machine, or made a mobile-phone call, you've used a database. In fact, many cities use intelligent traffic guiding system databases to control stoplights. So the next time you're waiting at a red light, it may be a database that is responsible for your delay!

* In this chapter, you will learn more about databases and how they're organized and created.

## 3. Relational Dabases

* A relational database allows tables to be related by means of a common filed. As few two tables can be considered a relational database if they share a common field.

<div style="margin: auto; width: 50%">

| COUNTRY_ID  | COUNTRY_NAME | REGION_ID |
| ----------- | -----------  | --------- |
| CA      | Canada | 2 |
| DE      | Germany | 1 |
| UK      | United Kingdom | 1 |
| US      | United States of America | 2 |

</div>

* Realistically, databases used in business have many tables, each table sharing a common field with another table.
* The `countries` table shown is one of several tables in the Employees database and just one example of the many tables that will be used in this course.

- To Understand how important databases have become in today's world, consider the following statistics:
    - Currently 20% of the world's data resides in `RDBMSs`
    - In the next two years, databases are expected to grow larger than 100 Tb.
    - A database this big would be able to store 100,000 copies of the Encyclopedia Britannica or 200,000 hours of music or about 10 billion web pages.

- Some of the top 10 world's larges databases using the Oracle RDBMS are:
    - France Telecom, 29.2Tb -- a communication company (a Tb is a terabyte equivalent to 1,000 gigabytes).
    - Amazon.com with 13 Tb.
    - The Claria Corporation, 12 Tb -- Internet behaviour marketing company tracking internet user behavior.

## 4. Review keys Terms

- Let's review the following key terms:
    - `table` -- basic storage structure.
    - `column` -- one kind of data in a table.
    - `row` -- data for one table instance
    - `field` -- the one value found at the intersection of a row and a column.
    - `primary key` -- unique identifier for each row.
    - `foreign key` -- column that refers toi a **primary-key** column in another table.

## 5. Properties of Tables

- There are six properties of tables in a relational table database:
    - *Property 1*: Entries in columns are **single-valued.**
    - *Property 2*: Entries in columns are of the same kind.
    - *Property 3*: Each row is unique.
    - *Property 4*: Sequence of columns is insignificant.
    - *Property 5*: Sequence of rows is insignificant.
    - *Property 6*: Each column has a unique name.

## 6. Accessing Data in an RDBMS

- A relational database-management system (RDBMS) organizes data into related rows and columns. To access the data in a database, you do not need to know where the data is located physically, you don't need to specify an access route to the tables.

- You simply use structured query language (SQL) statements and operators.

## 7. Communicating with Databases

- Working with the database is very similar to calling up and talking to a friend on the phone.
    - First, you muest choose a method to communicate `(the phone)`
    - Once connected, you ask your friend a question `(a query)`
    - In response to your question, your friend answers `(return of data)`

* As shown in the diagram, communicating with an RDBMS is accomplished by entering a SQL statement in Oracle Application Express

<div align="center">

![rdbms-schema](../../assets/img/rdbms-schema.PNG)

</div>

The request is then sent to the Oracle Server (a database running on a computer), tje request is processed and the data returned is displayed. In very large database systems, many users, servers and tables make up the RDBMS.

## 8. Categories of SQL statements

- SQL statements are grouped into several categories depending on the functions they perform. During this course, you will learn how to use SQL to execute these statements. The data retrieval statement retrieves data from the database using the keyword `SELECT`.

?> There are four main categories of `SQL` statements: <br/> 1. Data manipulation language `(DML)` <br/> 2. Data definition language `(DLL)` <br/> 3. Transaction control language `(TCL)` <br/> 4. Data control language `(DCL)`

<span style="color:#f2a350">*Data manipulation language, the `DML`*.</span> DML statements begin with `INSERT, UPDATE, DELETE` or `MERGE` and are used to modify the table data by entering new rows, changing existing rows, or removing,  existing rows.

<span style="color:#f2a350">*Data definition language `DDL`*</span>. Create, change and remove data structures from the database. They keywords `CREATE, ALTER, DROP, RENAME, TRUNCATE` begin DDL statements.

<span style="color:#f2a350">Transaction control language `TCL`</span>. Used to manage the changes made by DML statements. Changes to the data are executed using `COMMIT, ROLLBACK, SAVEPOINT`. TCL changes can be grouped together into logical transactions.

<span style="color:#f2a350">Data control language `DCL`</span>. Keywords `GRANT` and `REVOKE` are used to give or remove access rights to the database and the structures within it.

## 9. Terminology

- Key terms used in this lesson included:
    - Data control language (DCL)
    - Data definition language (DDL)
    - Data manipulation language (DML)
    - Filed
    - Foreign key
    - rdbms
    - Primary key
    - Relational Database
    - Row
    - Table
    - Transaction Control (TCL)

## 10. Summary

In this chapter, you should have learned how to:

- Define and give an example of a relational database
- Identify table-key terms, including row, column, field, primary, key and foreign key.
- Relate the importance of databases to everyday life.
