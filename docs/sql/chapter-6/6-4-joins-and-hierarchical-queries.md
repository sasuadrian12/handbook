# 6.4 Joins and hierarchical queries

## 1. Purpose

In data modeling, it was sometimes necessary to show an entity with a relationship to itself. For example, an employee can also be a manager. We showed this using the recursive or "pig's ear" relationship

<div align="center">

![data-mutiple-tables](../../assets/img/pig-s.PNG)

</div>

Once we have a self-join is used to join a table to itself as if it was two tables:

```sql
SELECT worker.last_name || ' works for ' || manager.last_name
AS "Works for"
FROM employees worker JOIN employees manager 
ON (worker.manager_id = manager.employee_id);
```

## 2. SELF-JOIN

To join a table to itself, the table is given two names or aliases. This will make the database "think" that there are two tables.

<h4>EMPLOYEES(workers)</h4>

<div align="center">

![data-mutiple-tables](../../assets/img/manager-worker.PNG)

</div>

?> Manager_id in the worker table is equal to employee_id in the manager table.

* Choose alias names that relate to the data's association with that table.

```sql
SELECT worker.last_name, worker.manager_id, manager.last_name
AS "Manager name"
FROM employees worker JOIN employees manager
ON (worker.manager_id = manager.employee_id);
```

<div style="margin: auto; width: 70%">

| LAST_NAME | MANAGER_ID | Manager name |
|-----------|------------|--------------|
| Kochhar   | 100        | King         |
| De Haan   | 100        | King         |
| Zlotkey   | 100        | King         |
| Mourgos   | 100        | King         |
| Whalen    | 101        | Kochhar      |

</div>

## 3. Hierarchical Queries

Closely related to self-joins are hierarchical queries. On the previous page, you saw how can use self-joins to see each employee's direct manager. With hierarchical queries, we can also see who that manager works for and so on. With this type of query we can build on Organization Chart showing the structure of a company or a department.

* Imagine a family tree with the eldest members of the family found close to the base or trunk of the tree and the youngest members representing branches of the tree. Branches can have their own branches and so on.
* 
<div align="center">

![data-mutiple-tables](../../assets/img/family.PNG)

</div>

* Using hierarchical queries, you can retrieve databases on a natural hierarchical relationship between rows in a table. A relational database does not store records in a hierarchical way. 
* However, where a hierarchical relationship exists between the rows of a single table, a process called tree walking enables the hierarchy to be constructed. A hierarchical query is a method of reporting the branches of a tree in a specific order.

## 4. Hierarchical Queries data

Examine the sample data from the `EMPLOYEES` table below and look at how you can manually make the connections to se who works for who starting with Steven King and moving through the tree from there.

<div align="center">

![data-mutiple-tables](../../assets/img/big-table.PNG)

</div>

## 5. Hierarchical Queries Illustrated

The organisation chart that we can draw from the data in the employees table will look like this:


<div align="center">

![data-mutiple-tables](../../assets/img/organisation-chart.PNG)

</div>

## 6. Hierarchical Queries keywords

* Hierarchical queries have their own new keywords:
  * Start WITH: identifies which row to use as the root for the tree it is constructing.
  * CONNECT BY PRIOR: explains how to do the inter-row joins.
  * LEVEL: specifies how many branches deep the tree will traverse.

!> Now, see in the internet some examples for each type of keyword.