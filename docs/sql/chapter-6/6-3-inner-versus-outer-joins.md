# 6.3 Inner Versus Outer Joins

## 1. Purpose

Up to now, all the joins returned data that matched the join condition. Sometimes, however, we want to retrieve both the data that meets the join condition and the data that does not meet the join condition. The outer joins in ANSI-99 SQL allows this functionality.

## 2. INNER and OUTER joins

In ANSI-99 SQL, a join of two or more tables that returns only the matched rows is called an inner join. When a join returns the unmatched rows as well as the matched rows, it is called an outer join. 

* Outer join syntax uses the terms "left, full and right". These names are associated with the order of the table names in the `FROM` clause of the `SELECT` statement

## 3. LEFT and RIGHT OUTER joins

In the example shown of a left outer join, note that the table name listed to the left of the words "left outer join" is referred to as the "left table."

```sql
SELECT e.last_name, d.department_id, d.department_name
FROM exmployees e LEFT OUTER JOIN
departments d
ON (e.department_id = d.department_id);
```

<div style="margin: auto; width: 80%">

| LAST_NAME | DEPT_ID | DEPT_NAME     |
|-----------|---------|---------------|
| Whalen    | 10      | Administrator |
| Fay       | 20      | Marketing     |
| Zlotkey   | 80      | Salse         |

</div>

This query will return all employee last names, both those that are assigned to a department and those that are not.

* This right outer joiner would return all department IDs and department names, both those that have employees assigned to them and those that do not.

## 4. FULL OUTER Join

It is possible to create a join condition to retrieve all matching rows and all unmatched rows from both tables. Using a full outer join solves this problem. The result set of a full outer join includes all rows from a left outer join and all rows from a right outer join combined without duplication.

```sql
SELECT e.last_name, d.department_id, d.department_name 
FROM employees e FULL OUTER JOIN departments d
ON (e.department_id = d.department_id);
```

## 5. Join scenario

Construct a join to display a list of employees, their current job_id and any previous jobs they may have held. The job_history table contains details of an employee's previous jobs.

```sql
SELECT last_name, e.job_id AS "Job", jh.job_id AS "OLD job", end_date
FROM employees e LEFT OUTER JOIN job_history jh
ON (e.employee_id = jh.employee_id);
```
<div style="margin: auto; width: 80%">

| LAST_NAME | Job       | Old job    | END_DATE     |
|-----------|-----------|------------|--------------|
| King      | AD_PRES   | -          | -            |
| Kochhar   | AD_VP     | AC_MGR     | 15-Mar-1997  |
| Kochhar   | AD_VP     | AC_ACCOUNT | 27-Oct-1993  |

</div>
