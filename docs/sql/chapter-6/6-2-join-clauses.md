# 6.2 Join clauses

## 1. Purpose

As you add more commands to your database vocabulary, you will be better able to design queries the return the desired result. The purpose of a join is to bind data together, across tables, without repeating all the data in every table. Why ask for more data that you really need?

## 2. USING Clause

In a natural join, if the tables have columns with the same names but different data types, the join causes an error. To avoid this situation, the join clause can be modified with a `USING` clause. The `USING` clause specified the column that should be used for the join.

* The query shown is an example of the `USING` clause.The column referenced in the `USING` clause should not have a qualifier (table name of alias) anywhere in the SQL statement.

```sql
SELECT first_name, last_name, department_id, department_name
FROM employees JOIN departments USING (department_id);
```

<div style="margin: auto; width: 80%">

| FIRST_NAME | LAST_NAME | DEPARTMENT_ID | DEPARTMENT_NAME |
|------------|-----------|---------------|-----------------|
| Jennifer   | Whalen    | 10            | Administration  |
| Michael    | Hartstein | 20            | Marketing       |
| Pat        | Fay       | 20            | Marketing       |

</div>

The `USING` clause allows us to use `WHERE` to restrict rows from one or both tables:

```sql
SELECT first_name, last_name, department_id, department_name
FROM exmployees JOIN departments USING (department_id)
WHERE last_name = 'Higgins'
```

<div style="margin: auto; width: 80%">

| FIRST_NAME | LAST_NAME | DEPARTMENT_ID | DEPARTMENT_NAME |
|------------|-----------|---------------|-----------------|
| Shelley    | Higgins   | 110           | Accounting      |

</div>

## 3. ON Clause

* What if the columns to be joined have different names, or if the join uses non-equality comparison operators such as < >, or `BETWEEN`?. 
* We can't use `USING`, so instead we use an `ON` clause. This allows a greater variety of join conditions to be specified. The `ON` clause also allows us to use `WHERE` to restrict rows from one or both tables.

## 4. ON Clause example

In this example, the ON clause is used to join the employees table with the jobs table:

```sql
SELECT last_name, job_title
FROM employees e JOIN jobs j on (e.job_id = j.job_id);
```
<div style="margin: auto; width: 80%">

| LAST_NAME | JOB_TITLE                    | 
|-----------|------------------------------|
| King      | President                    |
| Kochhar   | Administrator/Vice President |
| De Haan   | Administrator/Vice President |
| Whalen    | Administrator/Assistant      |

</div>

?> A join ON clause is required when the common columns have different names in the two tables.
* When using an `ON` clause on columns with the same name in both tables, you need to add a qualifier (either the table name or alias) otherwise an error will be returned.

```sql
SELECT last_name, job_title
FROM employees e JOIN jobs j ON (e.jobs_id = j.job_id)' 
```
<div style="margin: auto; width: 80%">

| LAST_NAME | JOB_TITLE                    | 
|-----------|------------------------------|
| King      | President                    |
| Kochhar   | Administrator/Vice President |
| De Haan   | Administrator/Vice President |
| Whalen    | Administrator/Assistant      |

</div>

* The example above uses table aliases as a qualifier `e.job_id = j.job_id` but could also have been written using the table names (employees.job_id = jobs.job_id).

## 5. ON Clause with WHERE Clause

Here is the same query with a `WHERE` clause to restrict the rows selected.

```sql
SELECT last_name, job_title
FROM employees e JOIN jobs j ON (e.job_id = j.job_id)
WHERE last_name LIKE 'H%' 
```

<div style="margin: auto; width: 80%">

| LAST_NAME | JOB_TITLE               | 
|-----------|-------------------------|
| Higgins   | Accounting Manager      |
| Hunold    | Programmer              |
| Hartstein | Marketing Manager       |

</div>

## 6. ON Clause with non-equality operator

* Sometimes you may need to retrieve data from a table that has no corresponding column in another table. Suppose we want to know the grade_lever for each employees salary. The job_grades table does not have a common column with the employees table. Using `ON` clause allows us to join the two tables.

```sql
SELECT last_name, salary, grade_lever, lowest_sal, highest_sal
FROM employees JOIN job_grades
ON (salary BETWEEN lowest_sal AND highest_sal)
```

<div style="margin: auto; width: 80%">

| LAST_NAME | SALARY | GRADE_LEVEL | LOWEST_SAL | HIGHEST_SAL |
|-----------|--------|-------------|------------|-------------|
| Vargas    | 2500   | A           | 1000       | 2999        |
| Matos     | 2600   | A           | 1000       | 5999        |
| Davies    | 3100   | B           | 3000       | 5999        |
| Rajs      | 3500   | B           | 3000       | 5999        |
| Lorentz   | 4200   | B           | 3000       | 5999        |
| Whalen    | 4400   | B           | 3000       | 5999        |

</div>

## 7. Joining Three tables.

* Both `USING` and `ON` can be used to join three or more tables. Suppose we need a report of our employees, their department and the city where the department is located? We need to join three tables: employees, departments and locations.

```sql
SELECT last_name, department_name
AS "Department", city
FROM employees JOIN departments USING (department_id)
JOIN locations USING (location_id);
```

<div style="margin: auto; width: 80%">

| LAST_NAME | Department | CITY      |
|-----------|------------|-----------|
| Hartstein | Marketing  | Toronto   |
| Fay       | Marketing  | Toronto   |
| Zlotkey   | Sales      | Oxford    |
| Abel      | Sales      | Oxford    |
| Taylor    | Sales      | Oxford    |
| Hunold    | Sales      | Southlake |

</div>


