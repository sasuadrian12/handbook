# 6.1 Cross Joins and Natural Joins

## 1. Purpose

Up to now, you experience using SQL has been limited to querying and returning information from one database table at a time. This would not be a problem if all dat ain the database were stored in only one table.

<div align="center">

![data-mutiple-tables](../../assets/img/data-mutiple-tables.PNG)

</div>

But you know from data modeling that separating data into individual tables and being able to associate the tables with one another is the heart of relational database design.<br/> Fortunately, SQL provides join conditions that enable information to be queried from separate tables and combined in one report.

## 2. Join Commands

* THere are two sets of commands or syntax which can be used to make connections between tables in a database:
  * Oracle proprietary joins.
  * ANSI/ISO SQL 99 compliant standards joins

* In this chapter, you will learn to use both sets of join commands. Oracle proprietary joins will be covered later in the course.

## 3. ANSI

`ANSI` stands for American National Standards Institute. Founded in 1918, ANSI is a private, non-profit organization that administers and coordinates the U.S voluntary standardization and conformity assessment system.

* The Institute's mission is to enhance both the global competitiveness of U.S business and the U.S quality of life by promoting and facilitating voluntary consensus standards and conformity assessment systems, and safeguarding their integrity.

## 4. SQL

Structured Query Language (SQL) is the information processing industry-standard language of relational database management systems (RDBMS). The language was originally designed by IBM in the mid 1970s, came into widespread use in the early 1980s, and become an industry standard in 1986 when it was adopted by ANSI. <br/><br/> So far there have been three ANSI standardizations of SQL, each one building one the previous one. They are named after the year in which they were first proposed and are widely known by their short names: ANSI-86, ANSI-92 and ANSI-99.

## 5. Natural JOIN

A SQL join clause combines fields from 2 (or more) tables in a relational database. A natural join is based on all columns in two tables that have the same name and selects rows from the two tables that have equal values in all matches columns.
* The employees table has a job_id column. This is a referance to the column of the same name in the jobs table.


<div align="center">

  ![join](../../assets/img/join.PNG)
  
</div>

As shown in the same code, when using a natural join, it is possible to join the tables without having to explicitly specify the columns in the corresponding table. However, the names and data types of both columns must be the same.

```sql
SELECT first_name, last_name, job_id, job_title 
FROM employees NATURAL JOIN jobs WHERE department_id > 80;
```
This join will return columns from the employees table and their related job_title from the jobs table based on the common column job_id.

```sql
SELECT first_name, last_name, job_id, job_title
FROM employees NATURAL JOIN jobs
WHERE department_id > 80;
```

<div style="margin: auto; width: 70%">

| FIRST_NAME | LAST_NAME | JOB_ID     | Mod Demo                      |
|------------|-----------|------------|-------------------------------|
| Steven     | King      | AD_PRES    | President                     |
| RNeena     | Kochhar   | AD_VP      | Administration Vice President |
| Lex        | De Haan   | AD_VP      | Administration Vice President |
| Shelley    | Higgins   | AC_MGR     | Account Manager               |
| William    | Gietz     | AC_ACCOUNT | Public Accountant             |

</div>

* Here is another example:

```sql
SELECT department_name, city
FROM departments NATURAL JOIN locations;
```

<div style="margin: auto; width: 50%">

| DEPARTMENT_NAME | CITY                |
|-----------------|---------------------|
| Marketing       | Toronto             |
| Sales           | Oxford              |
| It              | Southlake           |
| Shipping        | South San Francisco |
| Administration  | Seattle             |

</div>

## 6. CROSS JOIN

The ANSI/ISO SQL: 1999 SQL CROSS JOIN joins each row on one table to every row in the other table. The result set represents all possible row combinations from the two tables. This could potentially be very large!. If you `CROSS JOIN` a table with 20 rows with a table with 100 rows, the query will return 2000 rows.

**Example:**

The employees table contains 20 rows and the departments table has 8 rows. Performing a `CROSS JOIN` will return 160 rows.

```sql
SELECT last_name, department_name FROM employees CROSS JOIN departments;
```
<div style="margin: auto; width: 50%">

| LAST_NAME | DEPARTMENT_NAME   |
|-----------|-------------------|
| Abel      | Administration    |
| Davies    | Administration    |
| De Haan   | Administration    |
| Ernst     | Administration    |
| Fay       | Administration    |

</div>
