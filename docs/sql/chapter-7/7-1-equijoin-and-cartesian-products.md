# 7.1 Equijoin and Cartesian Products

## 1. Purpose

The previous section looked at querying and returning and returning data from more than one table in a relational database using ANSI/ISO SQL: 99 syntax. Legacy versions of Oracle databases required joins to use Oracle Propritary join syntax and many of these older databases are still in use.

## 2. Join Commands

* The two sets of commands or syntax which can be used to make connections between tables in a database:
  * Oracle proprietary joins
  * ANSI/ISO SQL: 99 compliant standard joins

## 3. Join Comparison 

Comparing Oracle proprietary  Joins with ANSI/ISO SQL: 1999 Joins.

<div style="margin: auto; width: 70%">

| Oracle Proprietary | ANSI/ISO SQL: 1999 Equivalent                                                                          |
|--------------------|--------------------------------------------------------------------------------------------------------|
| Cartesian Product  | Cross Join                                                                                             |
| Equijoin           | Natural Join </br></br> JOIN USING clause <br/></br> JOIN ON clause (if the equality operator us used) |
| Non-equijoin       | ON clause                                                                                              |

</div>

## 4. ORACLE Proprietary Joins

To query data from more than one table using the Oracle proprietary syntax, use a join condition in the `WHERE` clause. The basic format of a join statement is:

```sql
SELECT table.column, table2.column
FROM table1, table2
WHERE table1.column1 = table2.column2;
```
* Imagine the same problem arising from having two students in the same class with the same last name. When needing to speak to "Jackson", the teacher clarifies which "Jackson" by replacing the last name with the first name. To make it easier to read a JOIN statement and to speed up database access, it is good practice to preface the column name with the table name. 
```sql
SELECT table.column, table2.column
FROM table1, table2
WHERE table1.column1 = table2.column2; 
```

> - This is called "qualifying your columns" </br>
> - The combination of table name and column name helps eliminate ambiguous names when two tables contain a column with the same column name <br/>
> - When the same column name appears in both tables, the column name must be prefaced with the name of the table.

<h4>Example</h4>

To qualify the columns, you use the syntax tablename.columnname as shown in the example below.

```sql
SELECT table1.colummn, table2.column
FROM table1, table2
WHERE table1.column1 = table2.column2
```

## 5. Equijoin

* Sometimes called a simple or inner join, an equijoin is a table join that combines rows that have the same values for the specified columns.
* An equijoin is equivalent to ANSI:
  * NATURAL JOIN
  * JOIN USING
  * JOIN ON (when the join condition uses "=")

1) `What?` The SELECT clause specified the column names to display
2) `Where?` The FROM clause specified the tables that the database must access, separated by commas.
3) `How?` the WHERE clause specifies how the tables are to be joined
4) An equijoin uses the equals operator to specify the join condition.

<div align="center">

![data-mutiple-tables](../../assets/img/equijoin.PNG)

</div>

## 6. Aliases

* Working with lengthy column and table names can be cumbersome. Fortunately, there is a way to shorten the syntax using aliases. To distinguish columns that have identical names but reside in different tables, use table aliases. A table alias is similar to a column alias; it renames an object within a statement. It s created by entering the new name for the table just after the table name in the from-clause.

## 7. Talble aliases

Table aliases are used in the query below:

```sql
SELECT last_name, e.job_id, job_title
FROM employees e, jobs j
WHERE e.job_id = j.job_id
AND department_id = 80;
```

<div style="margin: auto; width: 70%">

| LAST_NAME | JOB_ID | JOB_TITLE            |
|-----------|--------|----------------------|
| Zlotkey   | SA_MAN | Sales Manager        |
| Abel      | SA_REP | Sales Representative |
| Taylor    | SA_REP | Sales Representative |

</div>

* When column names are not duplicated between two tables, you do not need to add the table name or alias to the column name.

* If a table alias is used in the FROM clause, then that table alias must be substituted for the table name throughout the SELECT statement. Using the name of a table in the SELECT clause that has been given an alias in the FROM clause will result in an error.

```sql
SELECT last_name, employees.job_id, job_title
FROM employees e, jobs j
WHERE e.job_id = jjob_id
AND department_id = 80;
```

<div align="center">

![data-mutiple-tables](../../assets/img/error-column-name.PNG)

</div>

!> Replace "NAME" with EMPLOYEES

## 8. Cartesian Product Join

* If two tables in a join query have no join condition specified in the `where` clause or the join condition is invalid, the Oracle server returns the cartesian product tof the two tables. This is a combination of each row of one table with each row of the other. A Cartesian product is equivalent to an ANSI CROSS JOIN. To avoind a cartesian product, always include a valid join condition in a `WHERE` clause.

In this query, the join condition has been omitted:

```sql
SELECT employees.last_name, departments.department_name
FROM employees, departments;
```

<div style="margin: auto; width: 70%">

| LAST_NAME | DEPARTMENT_NAME  |
|-----------|------------------|
| Abel      | Administration   |
| Davies    | Administration   |
| De Haan   | Administration   |

</div>

?> 160 rows returned in 0.01 seconds

## 9. Restricting rows in a join

As with single-table queries, the `WHERE` clause can be used to restrict the rows considered in one or more tables of the join. The query shown uses the `AND` operator to restrict the rows returned

```sql
SELECT employees.last_name, employees.job_id, jobs.job_title
FROM employees, jobs
WHERE employees.job_id = jobs.job_id
AND employees.department_id = 80;
```

<div style="margin: auto; width: 70%">

| LAST_NAME | JOB_ID | JOB_TITLE            |
|-----------|--------|----------------------|
| Zlotkey   | SA_MAN | Sales Manager        |
| Abel      | SA_REP | Perwakilan Penjualan |
| Taylor    | SA_REP | Perwakilan Penjualan |

</div>

* If you wanted to join three tables together, hwo many joins would it take? How many bridges are needed to join three islands? TO join three tables, you need to add another join condition to the WHERE clause using the AND operator.
* Suppose we need a report of our employees and the city where their department is located? We need to join three tables: employees, departments and locations

```sql
SELECT last_name, city
FROM employees e, departments d, locations l
WEHERE e.department_id = d.department_id 
AND d.location_id = l.location_id;
```

<div style="margin: auto; width: 70%">

| LAST_NAME | City    |
|-----------|---------|
| Hartstein | Toronto |
| Fay       | Toronto |
| Zlotkey   | Oxford  |

</div>