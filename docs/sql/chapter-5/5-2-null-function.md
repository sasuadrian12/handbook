# 5.2 Null function

## 1. Purpose

* Besides functions that control how data is formatted or converted to another type, SQL uses a set of general functions designed specifically to deal with null values.
* You may be wondering how a value that is unavailable, unassigned, unknown, or inapplicable can deserve so much attention.
* Null may be "nothing", but it can affect how expressions are evaluated, how averages are computed and where a value appears in a sorted list. This chapter is all about handling null values.

## 2. How functions are Evaluated

* Up to no, you have applied single-row functions in simple statements. It is possible however to nest functions to any depth. It is important to know how nested functions are evaluated. "Nesting" refers to one thing being contained within another thing (like an egg contained within a nest). The following example is a nested function.
* The evaluation process begins from the innermost lever to the outermost level.

```sql
SELECT TO_CHAR(NEXT_DAY(ADD_MONTHS(hire_date, 6), 'FRIDAY'), 'fmDay, Month ddth, YYYY') 
AS "Next Evaluation" from employees
WHERE employee_id = 100;
```

* The result is:
  * Friday, December 18th, 1987.

> **Step One**: The hire date is going to have siz months added to it.<br/><br/>
> **Step two**: The first Friday following the day returned at Step 1 will be identified.<br/><br/>
> **Step three**: The default date format will be formatted to read and display the date returned by Step 2 in a format similar to: Friday, December 18th, 1987 and will appear in the output under the column name "Next Evaluation"

## 3. Functions Pertaining to null values.

* At the beginning of the course, the term "null" was introduced. Null is the value that is unavailable, unassigned, unknown, or inapplicable. As a resul, we cannot test to see if it is the same as another value, because we do not know what value it has. It isn't equal to anything, **not even zero!**. But just bacause it really isn't anything doesn't mean that it is not important.

* Imagine this question: Is it true that X = Y?
* In Order to answer you have to know the values of X and Y. Oracle has four general functions that pertain to the use of null values. The four functions are:
  * NVL
  * NVL2
  * NULLIF
  * COALESCE

## 4. NVL Function

* The `NVL` function converts a null value to a known value of a fixed data type, either date, character or number. The data types of the null value column and the new value must be the same. The NVL function is:

```sql
NVL (expression 1 value that may contain a null, expression 2 value to substitute for null)
```

?> NVL (value or column that may contain a null, value to substitute for null).

* The following query uses the NVL function with character data types:

```sql
SELECT country_name, NVL(internet_extention, 'None')
AS "Internet extn"
FROM wf_countries
WHERE location = 'Southern Africa'
ORDER BY internet_extention DESC;
```

* Null values are replaced with the next 'None'

<div style="margin: auto; width: 50%">

| COUNTRY_NAME             | Internet extn |
|--------------------------|---------------|
| Juan de Nova Island      | None          |
| Europa Island            | None          |
| Republic of Zimbabwe     | .zw           |
| Republic of Zambia       | .zw           |
| Republic of South Africa | .za           |

</div>

* The data types of the null value column and the new value must be the same as shown in the following examples:

```sql
SELECT last_name, NVL(commission_pct,0)
FROM exmployees
WHERE deparment_id IN(80, 90)

-- Zlotkey -> .2
-- Abel -> .3
-- Taylor -> .2
-- King -> 0
```

```sql
SELECT NVL(date_of_independence,'No date')
FROM wf_countries;

-- 1-Jul-1867
-- 15-Sep-1821
-- 5-Jul-1975
-- NO DATE
```

?> **Node**: date_of_independence is a VARCHAR2 data type

* You can use the NVL function to convert column values containing nulls to a number before doing calculations. When an arithmetic calculation is performed with null, the result is null. The NVL function can convert the null value to a number before arithmetic calculations are done to avoid a null result. 

* In the example, the commission_pct column in the employees table contains null values. The NVL function is used to change the null to zero before arithmetic calculations.

```sql
SELECT last_name, NVL(commission_pct, 0) * 250 AS "Commission" FROM employees
WHERE department_id IN(80,90)L
```

<div style="margin: auto; width: 50%">

| LAST_NAME | Commission |
|-----------|------------|
| Zlotkey   | 50         |
| Abel      | 75         |
| Taylor    | 50         |
| King      | 0          |
| Kochhar   | 0          |
| De Haan   | 0          |

</div>

* The NVL2 function evaluates an expression with three values. If the first value is not null, then the NVL2 function returns the second expression. If the first value is null, then the third expression is returned. The values in expression 1 can have any data type. Expression 2 and expression 3 can have any data type except `LONG`.
* The data type of the returned value is always the same as the data type of expression 2, unless expression 2 is character data, in which case the returned type is `VARCHAR2`.

* The NVL2 function is:

```sql
NVL (expression 1 value that may contain a null, expression 2 value to return if expression 1
is not null, expression 3 value to replace if expression 1 is null).
```
* An easy way to remember NVL2 is to think, "if expression 1 has a value, substitute expression 2; if expression 1 is null, substitute expression 3."
* The NVL2 function shown uses number data types for expressions 1, 2 and 3.

```sql
SELECT last_name, salary,
NVL(commission_pct, salary + (salary * commission_pct), salary) AS 'income'
FROM employees
WHERE department_id IN(80, 90);
```

<div style="margin: auto; width: 50%">

| LAST_NAME | Commission | Commission |
|-----------|------------|------------|
| Zlotkey   | 10500      | 12600      |
| Abel      | 11000      | 14300      |
| Taylor    | 8600       | 10320      |
| King      | 24000      | 24000      |
| Kochhar   | 17000      | 17000      |
| De Haan   | 17000      | 17000      |

</div>

## 5. NULLIF Function

* The `NULLIF` function compares two expressions. If they are equal, the function returns null. If they are not equal, the function returns the first expression.
* The NULLIF function is:

```sql
NULLIF(expression 1, expression 2)
```

## 6. COALESCE functions

* The `COALESCE` function is an extension of the NVL function, except `COALESCE` can take multiple values. The word "coalesce" literally means "to come together" and that is what happens. If the first expression is null, expression is found. Of course, if the first expression has a value, the function returns the forst expression and the functions stops.
* The functions is:

```sql
COALESCE (expression 1, expression 2, ...expression n)
```

* Examine the SELECT statement from the employees table shown at right. If an employee has a value (not NULL) for commission_pct, this is returned, otherwise if salary has a value, return salary. If an employees commission_pct and salary are NULLm return the number 10:

```sql
SELECT last_name,
COALESCE(commission_pct, salary, 10) AS "Comm"
FROM employees ORDER BY commission_pct;
```

