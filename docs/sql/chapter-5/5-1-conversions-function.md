# 5.1 Conversions Function

## 1. Purpose

Imagine having to read all your school books in text files with no paragraphs and no capitalization. I would be difficult to read. Fortunately, there are software programs available to capitalize and color text, underline, bold, center and add graphics. For databases, format and display changes are done using conversion functions.
<br/>
These functions are able to display numbers as local currency, format dates in a variety of formats, display time to the second and keep track of what century a date refers to.

## 2. Data Types

* When a table is created for a database, the SQL programmer must define what kind of data will be stored in each field of the table.
* In SQL, there are several data types. These data types define the domain of values that each column can contain.
* For this chapter, you will use:
  * `VARCHAR2`
  * `CHAR`
  * `NUMBER`
  * `DATE`

## 3. Data types describer 

### 3.1 `VARCHAR2`
Used for character data of variable length, including numbers, dashes and special characters.

### 3.2 `CHAR`
Used for text and character data of fixed length, including numbers, dashes and special characters.

### 3.3 `NUMBER`
Used to store variable-length numeric data. No dashes, text or other non-numeric data are allowed. Currency is stored as a number data type.

### 3.4 `DATE`
Used for date and time values. Internally, Oracle stores dates as numbers and, by default, DATE information is displayed as DD-Mon-YYY (for example, 23-Oct-2013)

## 4. Type Conversion

The Oracle Server can automatically convert `VARCHAR2` and `CHAR` data to `NUMBER` and DATE data types. It can convert `NUMBER` and `DATE` data back to `CHARACTER` data type. This is known as type. This is known as implicit data conversion.


<div align="center">

![type-conversion](../../assets/img/type-conversion.PNG)

</div>

Although this is a convenient feature, it is always best to explicitly make data type conversions to ensure readability in SQL statements. <br/>

Implicit data type conversions

<div style="margin: auto; width: 50%">

| FROM              | TO       |
|-------------------|----------|
| VARCHARD2 or CHAR | NUMBER   |
| VARCHARD2 or CHAR | DATE     |
| NUMBER            | VARCHAR2 |
| DATE              | VARCHAR2 |

</div>

* The four data type conversion functions you will learn dare:
  * To convert date data type to character data type/
  * To convert number data type to character data type

<div align="center">

![explicit-data-types](../../assets/img/explicit-data-types.PNG)

</div>

## 5. Date conversion to Character Data

It is often desirable to convert a date from its default `DD-Mon-YYYY` format to another format specified by you. The function to accomplish this task is: 

```sql
TO_CHAR (date column name, 'format model you sepcify')
```

?>The `format model` must be enclosed in single quotation marks and is case-sensitive. Separate the date value from the format model with a comma. Aby valid date format element can be included.

* Use `sp` to spell out a number.
* Use `th` to have the number appear as an ordinal
  * (1st, 2nd, 2rd and so on)
* Use an `fm` element to remove padded blanks or remove leading zeroes from the output.

The table bellow show the different format models that can be used. When specifying time elements, note that hours (HH), minutes (MI), seconds (SS) and AM or PM can also be formated.

<div style="margin: auto; width: 50%">

|                |                                                  |
|----------------|--------------------------------------------------|
| YYYY           | Full year in numbers                             |
| YEAR           | Year spelled out                                 |
| MM             | Two-digit value for month                        |
| MONTH          | Full name of the month                           |
| MON            | Three-letter abbreviation of the month           |
| DY             | Three-letter abbreviation od the day of the week |
| DAY            | Full name of the day of the week                 |
| DD             | Numeric day of the month                         |
| DDspth         | FOURTEENTH                                       |
| Ddspth         | Fourteenth                                       |
| ddspth         | fourteenth                                       |
| DDD or DD or D | Day of year, month or week                       |
| HH24:MI:SS AM  | 15:45:32 PM                                      |
| DD "of" MONTH  | 12 of October                                    |

</div>

Example of output using different format models:

```sql
SELECT TO_CHAR(hire_date, 'fmMonth dd, YYYY') FROM employees;

-- June 07, 1994
```

```sql
SELECT TO_CHAR(hire_date, 'fmMonth dd, YYYY') FROM employees;

-- June 7, 1997
```

```sql
SELECT TO_CHAR(hire_date, 'fmMonth ddth, YYYY') FROM employees;
-- January 3rd, 1990
-- June 7th, 1994
```

```sql
SELECT TO_CHAR(hire_date, 'fmDay ddthsp Mon, YYYY') FROM employees;
-- Tuesday 7th Jun, 1994
```

```sql
SELECT TO_CHAR(hire_date, 'fmDay ddthsp "of" Month, Year') FROM employees;
-- Tuesday, seventh of June, Nineteen Ninety-Four
```

* Example of output using different format models for time:

```sql
SELECT TO_CHAR(SYSDATE, 'hh:mm') FROM DUAL;

-- 02:07
```

```sql
SELECT TO_CHAR(SYSDATE, 'hh:mm pm') FROM DUAL;

-- 02:07 am
```

```sql
SELECT TO_CHAR(SYSDATE, 'hh:mm:ss pm') FROM DUAL;

-- 02:07:23 am
```

## 6. Number conversion to Character Data (VARCHAR2)

* Numbers stored in the database have no formatting. This means that they have no currency signs/symbols, commas, decimals, or other formatting. To add formatting, you first need to convert the number to a character format:

```sql
TO_CHAR(number, 'format model')
```

* The SQL function that you use to convert a number to a desired character format is:

?> The table illustrates some of the format elements available to use with TO_CHAR

```sql
SELECT TO_CHAR(salary, '$99,999') as "Salary" FROM employees;
```
<div style="margin: auto; width: 50%">

| Salary  |
|---------|
| $24,000 |
| $17,000 |

</div>

<div style="margin: auto; width: 80%">

| Element | Description                                        | Example    | Result   |
|---------|----------------------------------------------------|------------|----------|
| 9       | Numeric position (# of 9's determine width)        | 999999     | 1234     |
| 0       | Display leading zeros                              | 099999     | 001234   |
| $       | Floating dollar sigh                               | $99999     | $1234    |
| L       | Floating local currency symbol                     | L99999     | FF1234   |
| .       | Decimal point in position specified                | 99999.99   | 1234.00  |
| ,       | Comma in position specified                        | 999,999    | 1,234    |
| MI      | Minus signs to right (negative values)             | 99999MI    | 1234-    |
| PR      | Parenthesize negative numbers                      | 999999PR   | <1234>   |
| EEE     | Scientific notation (must have four EEEE)          | 99.999EEEE | 1,23E+03 |
| V       | Multiply by 10 n times (n = number of 9's after V) | 9999V99    | 9999V99  |
| B       | Display zero values as blank, not 0                | B9999.99   | 1234.00  |

</div>

SQL Examples:

```sql
SELECT TO_CHAR(3000, '$99999.99') FROM DUAL

-- $3000.00
```

```sql
SELECT TO_CHAR(4500, '99,999') FROM DUAL;

-- 4,500
```

```sql
SELECT TO_CHAR(9000, '99,999.99') from DUAL;

-- 9,000.00
```

```sql
SELECT TO_CHAR(4422, '0009999') FROM DUAL;

-- 0004422
```

## 7. Character Conversion to Number

* It is often desirable to convert a character string to a number. The function for this conversion is:

```sql
TO_NUMBER(character string, 'format model')
```

* The format model is optional, but should be included if the character string being converted contains any characters other than numbers. You cannot reliably perform calculations with character data.

```sql
SELECT TO_NUMBER('5,320', '9,999') AS "Number" FROM DUAL;
```

<div style="margin: auto; width: 50%">

| Number |
|--------|
| 5320   |

</div>

* The bonus column includes data which contains 4 characters, the format model specifies 3 characters, so an error is returned.

```sql
SELECT last_name, TO_NUMBER(bonus, '999') FROM employees WHERE department_id = 80;

-- invalid number error
```

```sql
SELECT last_name, TO_NUMBER(bonus, '9999') AS "Bonus" FROM employees WHERE department_id = 80;
```

<div style="margin: auto; width: 50%">

| LAST_NAME | Bonus |
|-----------|-------|
| Zlotkey   | 1500  |
| Abel      | 1700  |
| Taylor    | 1250  |

</div>

## 8. Character Conversion to Date

* To convert a character string to a date format, use:

```sql
TO_DATE('character string', 'format model')
```

* This conversation takes a non-date value character string such as "November 3, 2001" and converts it to a date value.
* The format model tells the server what the character string "looks like":

```sql
TO_DATE('November 3, 2001', 'Month dd, yyyy')
```

- will return 03-Nov-2001

When making a character-to-date conversion, the `fx` (format exact) modifier specifies exact matching for the character argument and the date format model. In the following example, note that "May10" has no space between "May" and "10".
* The `fx` format model matches the character argument as it also has no space between "Mon" and "DD"

```sql
SELECT TO_DATE('May10, 1989', 'fxMonDD, YYYY') 
AS "Convert" FROM DUAL;
```

## 9. fx Modifier Rules

* The `fx` modifier rules are:
  * Punctuation and quoted text in the character argument must match the corresponding parts of the format model exactly (except for case)
  * The character argument cannot have extra blanks
    * Without fx, the Oracle Server ignores extra banks
  * Numeric data in the character argument must have the same number of digits as the corresponding element in the format model.
    * Without fx, numbers in the character argument can omit leading zeros.

<div style="margin: auto; width: 50%">

| Example                                                                  | Output      |
|--------------------------------------------------------------------------|-------------|
| SELECT TO_DATE('Sep 07, 1965', 'fxMon dd, YYYY') AS "Date" FROM DUAL;    | 07-Sep-1965 |
| SELECT TO_DATE('July3112004', 'fxMonthDDYYYY') AS "Date" FROM DUAL;      | 31-Jul-2004 |
| SELECT TO_DATE('June 19, 1990', 'fxMonth dd, YYYY') AS "Date" FROM DUAL; | 19-Jun-1990 |

</div>

## 10. Date format and YY Date Forma

All Date data should now be stored using four-digit years (YYYY). Some legacy databases however may still use the two-digit (YY) format. It has not been that long since the century changed from 1900 to 2000. Along with this change came considerable confusion whether a date written as 02-Jan-98 would be interpreted as January 2, 1998 or January 2, 2098.

## 11. RR Date format and YY date Format

* If the data being converted from character data to date data contains only a two-digit year, Oracle has a way of interpreting these dates in the correct century.
* For example: '27-Oct-95'

```sql
SELECT TO_DATE('27-Oct-95', 'DD-Mon-YY') AS "Date" from DUAL;
```
<div style="margin: auto; width: 50%">

| Date        |
|-------------|
| 27-Oct-2095 |

</div>

?> The two-digit year is interpreted as 2095, this may not be what was intended.

* If YY is used in the format model, the year is assumed to be in the current century. If the two-digit year is not in the current century, we use RR.

```sql
SELECT TO_DATE('27-Oct-95', 'DD-Mon-RR') AS "Date" from DUAL;
```
<div style="margin: auto; width: 50%">

| Date        |
|-------------|
| 27-Oct-1995 |

</div>

!> The two-digit year is now interpreted as 1995.

## 12. A few Simple rules.

* If the date format is specified with the RR format, the return value has two possibilities, depending on the current year. If the current year is between 00-49:
  * Dates from 0-49: The date will be in the current century
  * Dates from 50-99:
  * The date will be in the last century

<div align="center">

![date-rules](../../assets/img/date-rules.PNG)

</div>

* If the current year is between 50-99:
  * Dates from 0-49: The date will be in next century.
  * Dates from 50-99: The date will be in current century.

The table Bellow gives some examples of how YY and RR are interpreted, depending on the current year.

<div style="margin: auto; width: 50%">

| Current Year | Specified Date | RR Format | YY Format |
|--------------|----------------|-----------|-----------|
| 1995         | 27-Oct-95      | 1995      | 1995      |
| 1995         | 27-Oct-17      | 2017      | 1917      |
| 2017         | 27-Oct-17      | 2017      | 2017      |
| 2017         | 27-Oct-95      | 1995      | 2095      |

</div>

When I query my employee database using the following statement, it returns every row in the table. I know there are only a few employees who were hired before 1990

```sql
SELECT last_name, TO_CHAR(hire_date, 'DD-Mon-YY')
FROM employees
WHERE hire_date < TO_DATE('01-Jan-90','DD-Mon-YY');
```

As the format model in the `WHERE` clause uses YY, and the current year is 2017, the query returns rows with a hire_date less than 2090







