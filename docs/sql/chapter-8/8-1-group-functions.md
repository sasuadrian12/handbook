# 8.1 Group functions

## 1. Purpose 

* What if you were writing an article for the school newspaper and, to make a point, you wanted to know the average age of the students at your school? What would you have to do to get this information? You could ask each student their age in years, months and days, add up all of these numbers abd then divide by the number of students in your school.

* That would be one way --a very show and difficult way -- to find this information.

## 2. GROUP Functions

* In SQL, the following group functions can operate on a whole table or on a specific grouping of rows. Each function returns one result.

* Group functions:
  * AVG
  * COUNT
  * MIN
  * MAX
  * SUM
  * VARIANCE
  * STDDEV

## 3. GROUP functions List

* `MIN`: Used with columns that stored any data type to return the minimum value
* `MAX`: Used with columns that stored any data type to return the maximum value

<div align="center">

![img_1.png](../../assets/img/img.png)

</div>

* `SUM`: Used with columns that store numeric data to find the total of sum of values.
* `AVG`: Used with columns that stored numeric data to compute the average.
* `COUNT`: Returns the number of rows.
* `VARIANCE`: Used with columns that stored numeric data to calculate the spread of data around the mean
  * For example, if the average grade fot the class on the last test was 82% and the student's score ranged from 40% to 100%, the variance of scores would be greater than if the student's scores ranged from 78% to 88%.
* `STDDEV`: Similar to variance, standard deviation measures the spread of data
  * For two sets of data with approximately the same mean, the greater the standard deviation.

## 4. GROUP functions SELECT Clause.

* Group functions are written in the SELECT clause:

```sql
SELECT column, group_function(column),
...
FROM table
WHERE condition
GROUP BY column;
```

?> Group functions operate on sets of rows give one result per group.

<div align="center">

![img.png](../../assets/img/min-salary.png)

</div>

## 5. GROUP functions Cautions

* Important things you should know about group functions:
  * Group functions cannot be used in the WHERE clause:

```sql
SELECT last_name, first_name
FROM employees
WHERE salary = MIN(SALARY);
```

!> Will show an SQL error message.

* **Examples**

`MIN` Used with columns that store any data to return the minimum value.

```sql
SELECT MIN(life_Expect_at_birth) AS "Lowest lofe Exp"
FROM wf_countryes
```

?> Result: 32.62

```sql
SELECT MIN(country_name)
FROM wf_contries;
```

?> Result: Anguilla

```sql
SELECT MIN(hire_date)
FROM employees;
```

?> Result: 17-Jun-1987

* `MAX`: Used with columns that stored any data type to return the maximum value:

* `SUM`: Used with columns that sore numeric data to find the total or sum of values.

* `AVG`: Used with columns that stored numeric data to compute the average.

```sql
SELECT AVG(area)
FROM wf_countries
WHERE region_id = 29;
```

?> Result: 9656.96

```sql
SELECT ROUND(AVG(salary), 2)
FROM employees
WHERE department_id = 90;
```

?> Result: 19333.33

* `VARIANCE`: Used with columns that sore numeric data to calculate the spread of data round the mean.
* `SDDEV`: Similar to variance, standard deviation measures the spread of data.

```sql
SELECT ROUND(VARIANCE(life_expect_at_birth), 4)
FROM wf_countries;
```

?> Result: 143.2394

```sql
SELECT ROUND(STDDEV(life_expect_at_birth), 4)
FROM wf_countries;
```

?> Result: 11.9683

## 6. GROUP functions and NULL

* Group functions ignore NULL values. In the example bellow, the null values were not used to find the average `commision_pct`

```sql
SELECT AVG(commission_pct)
FROM employees;
```

<div style="margin: auto; width: 70%">

| AVG(COMMISION_PCT) |
|--------------------|
| .2125              |

</div>

<div style="margin: auto; width: 70%">

| LAST_NAME | COMMISSION_PCT |
|-----------|----------------|
| King      | -              |
| Kochhar   | -              |
| De Haan   | -              |
| Zlotkey   | .2             |
| Grant     | .15            |

</div>

## 7. More than one Group Function

You can have more than one group functions in the `SELECT` clause, on the same or different columns.

```sql
SELECT MAX(salary), MIN(salary), MIN(employee_id)
FROM employees
WHERE department_id = 60;
```

<div style="margin: auto; width: 70%">

| MAX(SALARY) | MIN(SALARY) | MIN(EMPLOYEE_ID) |
|-------------|-------------|------------------|
| 9000        | 4200        | 103              |


</div>

## 9. Rules for group functions

* Group functions ignore null values
* Group functions cannot be used in the WHERE clause
* MIN, MAX and COUNT can be used with any data type; SUM, AVG, STDDEV and VARIANCE can be used only  with numeric data types