# 2.1 Columns, Characters and Rows

## 1. Objectives

* This chapter covers the following objects:
    * Apply the concatenation operator to link columns to other column, arithmetic expressions, or constant values to create a character expression.
    * Use column aliases to rename columns in the query result.
    * Enter literal values of type character, number, or date into a `SELECT` statement.
    * Define and use `DISTINCT` to eliminate duplicate rows.
    * Edit, execute and save `SQL` statements on Oracle Application Express.

## 2. Purpose

- If you were writing an article about the Olympics, you might want to know many different countries and how many different athletes from each country were being represented. Having to go through lists and lists of participant names could be very tedious.

- Fortunately,, using SQL, your job could take less than a minute. In addition, you could format your output to read like a sentence. You will find these SQL features very useful.

## 3. Describe

- Use the `DESCRIBE` (DESC) command to display the structure of a table.

```sql
DESCRIBE <table_name>;
```

- `DESC` returns the table name, data types, primary and foreign keys and nullable columns, as well as other object details that will be covered later in the course. An example of using the `DESCRIBE`:

```sql
DESC departments;
```

<div>

| Table  | Column | Data type | Length | Precision | Scale | Primary key | Nullable | Default | Comment |
| ----------- | -----------  | --------- | --------- | --------- | --------- | --------- | --------- | --------- | --------- |
| DEPARTMENTS | DEPARTMENT_ID | NUMBER | - | 4 | 0 | 1 | - | - | - |
| | DEPARTMENT_NAME | VARCHAR2 | 30 | - | - | - | - | - | - |
| | MANAGER_ID | NUMBER | - | 6 | 0 | - | - | - | - |
| | LOCATION_ID | NUMBER | - | 4 | 0 | - | - | - | - |

</div>

?> This is important information when inserting new rows into a table because you must know the type of data each column will accept and whether or not the column can be left empty.

## 4. The concatenation Operator

Concatenation means to connect or link together in a series. The symbol for concatenation is 2 vertical bars sometimes referred to as `pipes`. Values on either side of the `||` operator are combined to make a single output column. 

The syntax is:

```sql
string1 || string2 || string_n
```

When values are concatenated, the resulting value is a character string.

- In sql, the concatenation operator can link columns to other columns, arithmetic expressions, or constant values to create a character expressions, or constant values to create a character expression. The concatenation operator is used to create readable text output. 

- In the following example, the `department_id` is concatenated to the `department_name`

```sql
SELECT department_id || department_name FROM departments;
```

<div style="margin: auto; width: 50%">

| DEPARTMENT_ID DEPARTMENT_NAME |
| ----------- |
| 1Administrator      |
| 2Marketing      |
| 3Shipping      |
| 4IT      |

</div>

In this variation on the previous example, the ` ||' '|| ` is used to make a space between the department_id and department_name. The space character in single quotation marks creates a space between the column values.

```sql
SELECT department_id ||' '|| department_name FROM departments;
```

<div style="margin: auto; width: 50%">

| DEPARTMENT_ID DEPARTMENT_NAME |
| ----------- |
| 1 Administrator      |
| 2 Marketing      |
| 3 Shipping      |
| 4 IT      |

</div>

Column aliases are useful when using the concatenation operator so that the default `SELECT` line does not appear as the column heading.

```sql
SELECT department_id ||' '|| department_name AS "Department Info" FROM departments;
```

<div style="margin: auto; width: 50%">

| Department info |
| --------------- |
| 1 Administrator      |
| 2 Marketing      |
| 3 Shipping      |
| 4 IT      |

</div>

```sql
SELECT first_name ||' '|| last_name AS "Employee Name" FROM employees;
```

<div style="margin: auto; width: 50%">

| Employee Name |
| --------------- |
| Ellen Abel      |
| Curtis Davies      |
| Lex De Haan      |

</div>

## 5. Concatenation and Literal Values

* A literal value is a fixed data value such as a character, number or date. The Following are examples of literal values:
    * 'dollars'
    * 1000
    * 'January 1, 2009'

* Using concatenation and literal values, you can create output that looks like a sentence or statement.

* Literal values can be included in the `SELECT` list with the concatenation operator. Characters and dates must be enclosed in a set of quotes ''.

* Every row returned from a query with literal values will have the same character string in it.

* In the following example, King earns 2400 dollars a month. The string 'has a monthly salary of 'and'dollars.', are examples of literals. If you where to create a SQL statement to produce output in the format, it would be written as:

```sql
SELECT last_name || ' has a monthly salary of ' || salary || ' dollars. ' AS "PAY" from employees;
```

<div style="margin: auto; width: 50%">

| PAY |
|-----|
|King has a monthly salary of 24000 dollars.|
|Kochhar has a monthly salary of 17000 dollars.|
|De Haan has a monthly salary of 17000 dollars.|
|Whalen has a monthly salary of 4400 dollars.|
|Higgins has a monthly salary of 12000 dollars.|
|Gietz has a monthly salary of 8300 dollars.|

</div>

?> Note the space character following the opening quote and preceding the ending quote.

:question: What happens if you remove the spaces?

You can also include numbers as literal values. In the following example, the number 1 is concatenated to the strings, 'has a 'and' year salary of'.

```sql
SELECT last_name || ' has a ' || 1 || ' year salary of ' || salary * 12 || ' dollars ' AS PAY FROM employees; 
```

<div style="margin: auto; width: 50%">

| PAY |
|-----|
| King has 1 year salary of 28800 dollars. |
| Kochhar has 1 year salary of 204000 dollars. |
| De Haan has 1 year salary of 204000 dollars. |
| Whalen has 1 year salary of 52800 dollars. |
| Higgins has 1 year salary of 144000 dollars. |

</div>

## 6. Using DISTINCT to Eliminate Duplicates Rows

Many times, you will want to know how many unique instances of something exist. For example, what if you wanted a list of all of the departments for which there are employees. You could write a query to select the department_ids from the employees table:

```sql
SELECT department_id FROM employees;
```

<div style="margin: auto; width: 50%">

| DEPARTMENT_ID |
|-----|
| 90 |
| 90 |
| 90 |
| 10 |
| 110 |
| 110 |
| 80 |
| 80 |
| 80 |
| 50 |

</div>

Unless you indicate otherwise, the output of a SQL query will display the results without eliminating duplicates rows. In SQL, the `DISTINCT` keyword is used to eliminate duplicate rows.

```sql
SELECT DISTINCT department_id FROM employees;
```

<div style="margin: auto; width: 50%">

| DEPARTMENT_ID |
|-----|
| 90 |
| 10 |
| 110 |
| 80 |
| 50 |

</div>

* The `DISTINCT` qualifier affects all listed columns and returns every distinct combination of the columns in the `SELECT` clause. The keyword `DISTINCT` must appear directly after the `SELECT` keyword.

## 7. EXECUTE, SAVE and EDIT in Oracle Application Express

Now that you have been using Oracle Application Express to create an execute statement, it would be nice if you could save those statements for later so you could run them again or perhaps edit them slightly and then save a new copy of the statement.

* Oracle Application Express has facilities to do just that. Your teacher will demonstrate these facilities for you and you can find further information on the Oracle Application Express User Guide.

## 8. Terminology

* Key terms used in this chapter included:
    * DESCRIBE
    * Concatenation Operator
    * Literal Values
    * DISTINCT

## 9. Summary

- In this lesson, you should have learned how to:
    - Apply the concatenation operator to link columns to other columns, arithmetic expressions or constant values to create a character expression.
    - Use column aliases to rename columns in the query result.
    - Enter literal values of type character, number or date into a `SELECT` statement.
    - Define and use `DISTINCT` to eliminate duplicate rows.
    - Edit, execute and save SQL statement in Oracle Application Express.