# 2.3 Comparison Operator

## 1. Objectives

- This chapter covers the following objectives:
    - Apply the proper comparison operator to return a desired result.
    - Demonstrate proper use of `BETWEEN`, `IN`, `LIKE` conditions to return a desired result.
    - Distinguish between zero and NULL, the latter of which is unavailable,  unassigned, unknown, or inapplicable.
    - Explain the use of comparison conditions and NULL.

## 2. Purpose

- We use comparisons in everyday conversation without really thinking about it.
    - "I can meet yoi `BETWEEN` 10:00 a.m and 11:00 am".
    - "I'm looking for a pair of jeans `LIKE` the ones you are wearing".
    - "If I remember correctly, the best concept seats are `IN` rows 100, 200 and 300".

* The need to express these types of comparisons also exist in SQL. Comparison conditions are used to find data in a table meeting certain conditions. Being able to formulate a `SELECT` clause to return specific data is a powerful feature of SQL.

## 3. Comparison Operators

- You are already familiar with the comparison operators such as equal to *(=)*, less than *(<)*, and greater than *(>)*. SQL has other operators that add functionality for retrieving specific sets of data. These include

    - BETWEEN...AND
    - IN
    - LIKE

## 4. BETWEEN...AND

The `BETWEEN...AND` operator is used to select and display rows based on a range of values. When used with the `WHERE` clause, the `BETWEEN...AND` condition will return a range of clause between and inclusive of the specific lower and upper limits.

Note in the example from the Employees database, the values returned include the lower-limit value and the upper-limit value. Values specified with the `BETWEEN` condition are said to be inclusive.

```sql
SELECT last_name, salary FROM employees WHERE salary BETWEEN 9000 AND 11000
```

<div style="margin: auto; width: 50%">

| LAST_NAME  | SALARY |
| ----------- | ----------- |
| Zlotkey      | 10500 |
| Abel      | 11000 |
| Hunold      | 9000 |

</div>

?> Note that the output included the lower-limit and upper-limit values.

Using `BETWEEN...AND` is the same as using the following expression:

```sql
WHERE salary >= 900 AND salary <= 11000;
```

In fact, there is no performance benefit in using one expression over the other. We use `BETWEEN...AND` for simplicity in reading the code.

## 5. IN

The `IN` condition is also known as the *"membership condition"*. It is used to test whether a value is `IN` a specified set of values. For example, `IN` could ne used to identify students whose identification number are 2349, 7354 or 4333 or people whose international phone calling code is 1735, 82 or 10.

```sql
SELECT city, state_province, country_id FROM locations WHERE country_id IN ('UK', 'CA');
```

<div style="margin: auto; width: 50%">

| CITY  | STATE_PROVINCE | COUNTRY_ID |
| ----------- | ----------- | ----------- |
| Toronto      | Ontario | CA |
| Oxford      | Oxford | UK |

</div>

In this example, the `WHERE` clause could also be written as a set of `OR` conditions:

```sql
SELECT city, state_province, country_id FROM locations
WHERE country_id IN ('UK', 'CA')

--OR

WHERE country_id = 'UK' OR country_id = 'CA'
```

As with `BETWEEN...AND`, the `IN` condition can be written using either syntax just as efficiently.

## 6. LIKE

Have you ever gone shopping to look for something that you saw in a magazine or on television but you weren't sure of the exact item? It's much the same with database searches.

A manager may know that an employee's last name starts with "S" but doesn't know the employee's entire name. Fortunately, in SQL, the `LIKE` condition allows you to select rows that match wither characters, dates, or number patterns.

Two symbols -- the *(%)* and underscore (_) -- called wildcard characters, can be used to construct a search string.

- The percent *(%)* symbol is used to represent any sequence of zero or more characters.
- The underscore (_) symbol is used to represent a single character.

In the example shown below, all employees with last names beginning with any letter followed by an "o" and then followed by any other number of letters will be returned.

```sql
SELECT last_name FROM employees WHERE last_name LIKE '_o%';
```

<div style="margin: auto; width: 30%">

| LAST_NAME  |
| ----------- |
| Kochhar      |
| Lorentz      |

</div>

> :exclamation: One additional that is important:
> When you need to have an exact match for a string that has a *%* or *_* character in it, you will need to indicate that the % or the _ is not a wildcard but is part of the item you're searching for.

Without the `ESCAPE` option, all employees that have an ***R*** in their JOB_ID would be returned.

```sql
SELECT last_name, job_id
FROM employees
WHERE job_id LIKE '%_R%';
```

<div style="margin: auto; width: 30%">

| LAST_NAME  | JOB_ID |
| ----------- | ----------- |
| Abel      | SA_REP |
| Atkinson      | ST_CLERK |
| Austin      | SA_REP |
| Baer      | PR_REP |
| Baida      | PU_CLERK |
| Banda      | SA_REP |
| Bates      | SA_REP |
| Bell      | SH_CLERK |
| Bernstein      | SA_REP |

</div>

## 7. IS NULL, IS NOT NULL

- Remember NULL? It is the value that is unavailable, unassigned, unknown or inapplicable.
- Being able to test for NULl is often desirable. You may want to know all the dates in June that, right now, do not have a concert scheduled. You may want to know all of the clients who do not have phone numbers recorded in your database.

- The `IS NULL` condition tests for navailable, unassigned, unknown data.
- `IS NOT NULL` tests for data that is available in the database. In the example, the `WHERE` clause is written to retrieve all the last names of those employees who do not have a manager.

```sql
SELECT last_name, manager_id FROM employees
WHERE manager_id IS NULL;
```

<div style="margin: auto; width: 30%">

| LAST_NAME  | MANAGER_ID |
| ----------- | ----------- |
| King      |  |

</div>

?> Employee KING is the President of the company, so has no manager

```sql
SELECT last_name, commission_pct, FROM employees
WHERE commission_pct IS NOT NULL;
```

<div style="margin: auto; width: 30%">

| LAST_NAME  | COMMISSION_PCT |
| ----------- | ----------- |
| Zlotkey      | .2 |
| Abel      | .3 |
| Taylor      | .2 |
| Grant      | .15 |

</div>

- `IS NOT NULL` returns the rows that have a value in the commission_pct column.


## 8. Terminology

- Key terms used in this chapter included:
    - BETWEEN...AND
    - IN
    - LIKE
    - IS NULL
    - IS NOT NULL

## 9 Summary

- In this chapter, you should have learned how to:
    - Apply the proper comparison operator to return a desired result.
    - Demonstrate proper use of `BETWEEN`, `IN` and `LIKE` conditions to return a desired result.
    - Distinguish between zero and NULL, the latter of which is unavailable, unassigned, unknown, or inapplicable.
    - Explain the use of comparison conditions and NULL