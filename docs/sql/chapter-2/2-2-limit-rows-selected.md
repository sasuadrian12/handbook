# 2.2 Limit Rows Selected

## 1. Objectives

- This chapter covers the following objectives:
    - Apply SQL syntax to restrict the row returned from a query.
    - Demonstrate application of the `WHERE` clause syntax.
    - Explain why it is important, from a business perspective, to be able to easily limit data retrieved from a table.
    - Construct and produce output using a SQL query containing character string and date values.

## 2. Purpose

- Have you ever had "information overloaded"?
- The television is on, your Mom is asking you how school went today, the phone rings and the dog is barking. Wouldn't it be nice to be able to restrict the amount of information you have to process at one time? In SQL, this is the job of the `WHERE` clause.

- It is important to be able to choose the information you need to see from a table.
- Tables can have millions of rows of data and it is waste of resources to search and return data you don't need or want.

## 3. SELECT statement

You use `SELECT` to retrieve information from the database. A `SELECT` statement must include at a minimum a `SELECT` clause and a `FROM` clause. The `WHERE` clause is optional.

```sql
SELECT * | { [DISTINCT] column | [expression alias]..} FROM table [WHERE condition(s)];
```

## 4. Where Clause

- When retrieving data from the database, you may need to limit the rows of data that are displayed. You can accomplish this using the `WHERE` clause. A WHERE clause contains a condition that must be met and it directly follows the `FROM` clause in a SQL statement.

- The syntax for the WHERE clause is:

```sql
WHERE column_name comparison_condition comparison_value
```

?> Note: An alias cannot be used in the `WHERE` clause.

Examine the following SQL statement from the Employees database:

```sql
SELECT emplyee_id, first_name, last_name FROM employees;
```

<div style="margin: auto; width: 50%">

| EMPLOYEE_ID  | FIRST_NAME | LAST_NAME |
| ----------- | -----------  | --------- |
| 100      | Steven | King |
| 101      | Neena | Kochhar |
| 102      | Lex | De Haan |

</div>

By adding `WHERE` clause, the rows are limited to those rows where the value of employee_id is 101

```sql
SELECT employee_id, first_name, last_name FROM employees WHERE employee_id = 101;
```

<div style="margin: auto; width: 50%">

| EMPLOYEE_ID  | FIRST_NAME | LAST_NAME |
| ----------- | -----------  | --------- |
| 101      | Neena | Kochhar |

</div>

## 5. Comparison Operators in the WHERE Clause

- As you saw in the previous examples, the = sign can be used in the `WHERE` clause. In addition to the "equal to" operator (=), other comparison operators can be used to compare one expression to another:
    - `=` equal to
    - `>` greater than
    - `>=` greater than or equal to
    - `<` less than
    - `<=` less than or ewual to
    - `<>` not equal to (or != or ^=)

In the example below, the department_id column is used in the `WHERE` clause, with the comparison operator `=`. All employees with a department_id of 90 are returned.

```sql
SELECT employee_id, last_name, department_id FROM employees WHERE  department_id = 90;
```

<div style="margin: auto; width: 50%">

| EMPLOYEE_ID  | LAST_NAME | DEPARTMENT_ID |
| ----------- | -----------  | --------- |
| 100      | King | 90 |
| 101      | Kochhar | 90 |
| 102      | De Haan | 90 |

</div>

## 6. Character and Date String in the WHERE clause

- Character string and dates in the `WHERE` clause must be enclosed in single quotation marks ''. Numbers, however, should not be enclosed in single quotation mark.

```sql
SELECT first_name, last_name FROM employees WHERE last_name = 'Taylor';
```

What do you think wil happen if the `WHERE` clause is written as:

```sql
WHERE last_name = 'jones';
```

All characters searches are case-sensitive. Because the employees table stores all the last names in the proper case, no rows are returned in this example. 

!> In another lesson, you will learn to use other SQL keywords `UPPER`, `LOWER` and `INITCAP` that will make it easier to avoid a case-sensitive mistake.

- Comparison operators can be used in all of the following ways in the where clause"

```sql
WHERE hire_date < '10-Jan-2000'
```

```sql
WHERE salary >= 6000
```

```sql
WHERE job_id = 'IT_PROG'
```

:question: In the following example from the Employees database, which rows will be selected?

:question: Will salaries of 3000 be included in the result set?

```sql
SELECT last_name, salary FROM employees WHERE salary <= 3000;
```

## 7. Terminology

- Key terms used in this chapter included:
    - WHERE Clause
    - Comparison Operators

## 8. Summary

- In this lesson, you should have learned how to:
    - Apply SL syntax to restrict the rows returned from a quesry.
    - Demonstrate application of the WHERE clause syntax.
    - Explain why it is important, from a business perspective to be able to easily limit data retrieved from a table.
    - Construct and produce output using a SQL query containing character strings and date values. 