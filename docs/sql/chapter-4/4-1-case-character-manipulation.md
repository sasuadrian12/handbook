# 4.1 Case character manipulation

## 1. Objects

* This chapter will cover the following objectives:
  * Select and apply single-row functions that perform case conversion and/or character manipulation.
  * Select and apply character case-manipulation functions `LOWER`, `UPPER`, `UNITCAP` in a SQL query.
  * Select and apply character-manipulation functions `CONCAT`, `SUBSTR`, `LENGTH`, `INSTR`, `LPAD`, `RPAD`, `TRIM` and `REPLACE` in a SQL query
  * Write flexible queries using substitution variables.

## 2. Purpose

Being able to change the way in which data is presented is important when dealing with data from a database. Most of the time in SQL, we need to change the way the data appears depending on the requirements of the task we are trying to accomplish. In this lesson, you will learn several ways in which to transform data to fit a particular situation.

## 3. DUAL Table

The DUAL table, has one row called "X" and one column called "DUMMY".

<div style="margin: auto; width: 50%">

| DUMMY |
|-------|
| X     |

</div>

The `DUAL` table is used to create `SELECT` statements and execute functions not directly related to a specific database table. Queries using the `DUAL` table return one row as a result. `DUAL` can be useful to do calculations and also to evaluate expressions that are not derived from a table.

* Dual will be used to learn many of the single-row functions. In this example, the DUAL table is used to execute a `SELECT` statement that contains a calculation. As you can see, the `SELECT` statement returns a value that does not exist in the DUAL table. The value returned is a result of the calculation result.

```sql
SELECT (319/29) + 12
FROM DUAL;
```
<div style="margin: auto; width: 50%">

| (319/29)+12 |
|-------------|
| 23          |

</div>

## 4. Single-row character functions

* Single-row character functions are divided into two categories:
  * functions that convert the case of character strings.
  * functions that can join, extract, find, pad abd trim character strings.

* Single-row functions can be used in the `SELECT`, `WHERE` and `ORDER BY` clause.

## 5. Case-manipulation functions
Case-manipulation functions are important because you may not always know in which case (upper, lower, or mixed) the data is stored in the database.

Case manipulation allows you to temporarily convert the database data to a case of your choosing. Mismatches between database case storage and query case requests are avoided.

Case-manipulation functions are used to convert data from the state it is stored in a table lower, upper or mixed case. These conversions can be used to format the output and can also be used to search for specific strings.

* CHARACTER FUNCTIONS
  * Case-manipulation functions
    * LOWER
    * UPPER
    * UNITCAP
  * Character-manipulation functions
    * CONCAT
    * SUBSTR
    * LENGTH
    * INSTR
    * LPAD | RPAD
    * TRIM
    * REPLACE

Case-manipulation functions cam be used in most parts of a `SQL` statement.

Case-manipulation functions are often helpful when uou are searching for data and you do not know whether the data you are logging for is in upper or lower case. From the point of view of the database, "V" and "v" are NOT the same character and, as such, you need to search using the correct case. `LOWER` (column | expression) converts alpha characters to lower-case.

```sql
SELECT last_name
FROM employees
WHERE LOWER(last_name) = 'abel';
```

* `UPPER` (column | expression) converts alpha characters to upper-case.

```sql
SELECT last_name
FROM employees
WHERE UPPER (last_name) = 'ABEL';
```
* `UNITCAP` (column | expression) converts alpha character values to uppercase for the first letter of each word.

```sql
SELECT last_name
FROM employees
WHERE UNITCAP(last_name) = 'Abel';
```

Character-manipulation functions are used to extract, change, format or alter in some way a character string. One or more characters or words are passed into the functions and the functions will then perform its functionality on the input character strings and return the changed, extracted, counted or altered value.

## 6. Character manipulation functions.

* `CONCAT`: joins two values together. Takes 2 character string arguments and joins the second string to the first. Could also be written using the concatenation operator - 'Hello' || 'World'

<div style="margin: auto; width: 50%">

| Examples                                             | Result                     |
|------------------------------------------------------|----------------------------|
| SELECT CONCAT('Hello', 'World') FROM DUAL;           | HelloWorld                 |
| SELECT CONCAT(first_name, last_name) FROM employees; | EllenAbel<br/>CurtisDavies |                   |

</div>

* `SUBSTR`: Extracts a string of a determined length. The arguments are (character String, starting position, length). The length argument is optional and if omitted, returns all characters to the end of the string.

```sql
SELECT SUBSTR('HelloWorld', 1,5)
FROM DUAL

--Hello
```

```sql
SELECT SUBSTR('HelloWorld', 6)
FROM DUAL;

--World
```

```sql
SELECT SUBSTR(last_name, 1, 3)
FROM employees;

--ABE
--DAV
```

* `LENGTH` shows the length of a string as a number value. The function takes a character string as an argument, and returns the number of characters in that character string. 

```sql
SELECT LENGTH('HelloWorld')
FROM DUAL;

--10
```

```sql
SELECT LENGTH(last_name) 
FROM employees;

--4
--6
...
```

* `INSTR` finds the numeric position of the specified character(s). INSTR searches for the first occurrence of a substring within a character string and returns the position as a number. If a substring is not found, the number zero is returned.

```sql
SELECT INSTR('HelloWorld', 'W')
FROM DUAL;

--6

SELECT last_name, INSTR(last_name, 'a')
FROM employees;

--Abel 0
-- Davies 2
```

* `LPAD` pads the left side of a character string, resulting in a right-justified value. Requires 3 arguments: 
  * a character string, the total number of characters in the padded string 
  * the character to pad with.

```sql
SELECT LPAD('HelloWorld', 15 '-')
FROM DUAL;

--Result -----HelloWorld

SELECT LPAD(last_name, 10, '*')
FROM emplloyees;

--******ABEL
--****Davies
...
```

* `RPAD` pads the right-hand side of a character string, resulting in the left-justified value.

```sql
SELECT RPAD('HelloWorld', 15, '-')
FROM DUAL;

--HelloWorld-----

SELECT RPAD(last_name, 10, '*')
FROM employees;

--Abel******
--Davies****
```

* `TRIM` removes all specified characters from either the beginning, the end or both beginning and end of a string. The syntax for the trim function is:

```sql
SELECT TRIM(LEADING'a'FROM'abcba')
FROM DUAL;

--bcba

SELECT TRIM(TRAILING'a'FROM'abcba')
FROM DUAL;

--abcb

SELECT TRIM(BOTH'a'FROM'abcba')
FROM DUAL;

--bcb
```

* `REPLACE` replaces a sequence of characters in a string with another set of characters. The syntax for the REPLACE function is:

```sql
REPLACE (string1, string_to_replace, [replacement_string])
```

  * **string1** is the string that will have characters replaced in it.
  * **string_to_replace0** is the string that will be searched for and taken out of string1
  * **[replacement_string]** is the new string to be inserted in string1.

```sql
SELECT REPLACE('JACK and JUE','J', 'BL')
FROM DUAL;

--BLACK and BLUE

SELECT REPLACE('JACK and JUE', 'J')
FROM DUAL;

--ACK and UE

SELECT REPLACE(last_name, 'a', '*')
FROM employees;

--Abel
--D*vies
De H**n

```

## 7. Using column aliases with functions

All functions operate on values that are in parentheses and each function name denotes its purpose, which is helpful to remember when constructing a query. Often a column alias is used to name a function. When a column alias is used, the column alias appears in the output instead of the actual function syntax.

* In the following examples, the alias "User Name" has replaced the function syntax in the first query. By default, the column name in a `SELECT` statement appears as the column heading. In the second query example, however, there is no column in the table for the results produced, so the query syntax is used instead.

```sql
SELECT LOWER(last_name) || LOWER (SUBSTR(first_name, 1, 1)) AS "User Name"
FROM employees;
```

<div style="margin: auto; width: 50%">

| User Name |
|-----------|
| abele     |
| daviesc   |
| de haanl  |

</div>

```
SELECT LOWER (last_name) || LOWER(SUBSTR(first_name, 1, 1))
FROM f_staffs;
```

<div style="margin: auto; width: 50%">

| query   |
|---------|
| does    |
| millerb |
| tuttlem |

</div>

## 8. Substitution variables

* Occasionally you may need to run the same query with many different values to get different result sets. Imagine for instance if you had to write a report of employees and their departments, but the query must only return data for one department at a time. Without the use of substitution variables, this request would mean you would have to repeatedly edit the same statement to change the `WHERE` clause.

* Luckily for us, Oracle Application Express supports substitution variables. To use them, all you have to do is replace the hardcoded value in your statement with a `:name_variable`. Oracle Application Express will then ask you for a value when you execute your statement.

* If this was the original query:

```sql
SELECT fist_name, last_name, salary, department_id
FROM employees
WHERE department_id = 10;
```

?> Then run it again with different values: 20, 30, 40... etc.

* It could be rewritten as;

```sql
SELECT first_name, last_name, salary,  department_id 
FROM employees
WHERE department_id =:enter_dept_id;
```

?> Note the use of `:` in front of enter_depth_id

It is the colon that is the magic bit and makes Oracle Application Express recognise the text that follows as a variable.

Substitution variables are treated as character string on Oracle Application Express, which means that when passing in character od date values, you do not need the single quotation marks that you would normally use to enclose the strings. So a `WHERE` clause would look like this:

```sql
SELECT *
FROM employees
WHERE last_name = :l_name;
```

## 9. Terminology

* Key terms used in this lesson included:
  * Character functions
  * CONCAT
  * DUAL
  * Expression
  * Format
  * INITCAP
  * Input
  * INSTR
  * LENGTH
  * LOWER
  * LPAD
  * Output
  * REPLACE
  * RPAD
  * Single-row functions
  * SUBSTR
  * TRIM
  * UPPER
  * Substitution variable

## 10. Summary

* In this chapter you should have learned how to:
  * Select and apply single-row functions that perform case conversion and/or character manipulation.
  * Select and apply character case-manipulation functions LOWER, UPPER and UNITCAP in a SQL query
  * SELECT and apply character-manipulation functions.
  * Write flexible queries using substitution variables.




