# 4.3 Date Functions

## 1. Objectives

* This chapter covers the following objectives:
  * Demonstrate the use of `SYSDATE` and date functions.
  * Sate the implications for world businesses to be able to easily manipulate data stored in date format.

## 2. Purpose

* Have you ever wonder how many days remains int he schools year or how many weeks there are until graduations? Because the Oracle database stores dates as numbers, you can easily perform calculations on dates using addition, subscription and other mathematical operators

* Businesses depend on being able to use date functions to schedule payrolls and payments, track employee performance reviews and years of service or keep track of orders and shipments.
* All of these business needs are easily handled using simple SQL date functions. 

## 3. Displaying Dates 

The default display and input format for dates is: 

```example
DD-Mon-YYYY
```
Ex: 02-Dec-2014

However, the Oracle database stores dates internally with a numeric format representing the century, year, month, day, hour, minute and second.

Valid Oracle dates are between January 1, 4712 B.C and December 30, 9999 A.D. This represents the range of dates that you can store successfully in a Oracle database.

## 4. SYSDATE

`SYSDATE` is a date function that returns the current database server date and time. Use `SYSDATE` do display the current date, use the `DUAL` table.

```sql
SELECT SYSDATE
FROM DUAL;
```

<div style="margin: auto; width: 50%">

| LAST_NAME   |
|-------------|
| 01-Jul-2017 |

</div>

## 7. DATE Data Type

The DATE data type always stores year information as a four-digit number internally: two digits for the century and two digits for the year. For example, the Oracle database stores the year as 1996 or 2004, not just as 96 or 04. In previous versions, the century component was not displayed by default. 

However, due to changing business requirements around the world, the 4-digit year is now the default display.

## 8. Working with Dates.

### a. Example 1:

```sql
SELECT last_name, hire_date + 60
FROM employees;
```

**Description 1** <br/>
Adds 60 days to hire_date.

### b. Example 2:

```sql
SELECT last_name, (SYSDATE - hire_Date)/7
FROM emplyees;
```
**Description 2** <br/>
Displays the number of weeks since the employee was hired.

### c. Example 3:

```sql
SELECT employee_id, (end_date - start_Date)/365
AS "Tenure in last job"
FROM job_history;
```
**Description 3** <br/>
Finds the number of days employee held a job, then divides by 365 to display in years.

## 9. Date functions

The date functions shown in the table operate on Oracle dates. All the date functions return a value with a `DATE` data type except the `MONTHS_BETWEEN` functions, which returns a numeric data type value.

<div style="margin: auto; width: 100%">

| Function       | Description                                              |
|----------------|----------------------------------------------------------|
| MONTHS_BETWEEN | Number of months between two dates                       |
| ADD_MONTHS     | Add calendar months to date                              |
| NEXT_DAY       | Date of the next occurrence of day of the week specified |
| LAST_DAY       | Last day of the month                                    |
| ROUND          | Round date                                               |
| TRUNC          | Truncate date                                            |

</div>

`MONTHS_BETWEEN`: takes 2DATE arguments and returns the number of calendar months between the 2 dates. If the first argument is an earlier date than the second, the number returned is negative.

<div style="margin: auto; width: 100%">

| Date function Example:                                                                                  | Result                                                                        |
|---------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------|
| SELECT last_name, hire_date <br/> FROM employees WHERE <br/> MONTHS_BETWEEN (sysdate, hire_Date) > 240; | King -> 17-Jun-1987 <br/> Kochhar -> 21-Sep-1989 <br/> De Haan -> 13-Jan-1993 |

</div>

`ADD_MONTHS`: takes 2 arguments, a DATE and a number. Returns a DATE value with the number argument added to the month component of the date. If the number supplied is negative, the function will subtract the number of months from the date argument.

```sql
SELECT ADD_MONTHS (SYSDATE, 12) AS "Next Year" FROM DUAL;
```

```Result -> 01-Jul-2016```

`NEXT_DAY`: takes 2 arguments, a DATE and a weekday and returns the DATE of the next occurrence of that weekday after the DATE argument.

```sql
SELECT NEXT_DAY(SYSDATE, 'Saturday') AS "Next Saturday" from DUAL;
```

`Result -> 04-Jul-2017`

`LAST_DAY`: takes a DATE argument and returns the DATE of the last day of the month for the DATE argument.

```sql
SELECT LAST_DAY(SYSDATE) AS "End of the Month" from DUAL;
```

`Result-> 31-Jul-2017`

`ROUND` returns a DATE rounded to the unit specified by the second argument.

```sql
SELECT hire_date,
ROUND(hire_date, 'Month')
FROM employees
WHERE department_id = 50;
```

<div style="margin: auto; width: 100%">

| HIRE_DATE  | ROUND(HIRE_DATE, 'Month') |
|------------|---------------------------|
| 2004-07-18 | 2004-08-01                |
| 2005-04-10 | 2005-04-01                |
| 2003-05-01 | 2003-05-01                |

</div>

Another example:

```sql
SELECT hire_date, ROUND(HIRE_DATE, 'Year')
FROM employees
Where department_id = 50;
```

<div style="margin: auto; width: 100%">

| HIRE_DATE  | ROUND(HIRE_DATE, 'Year') |
|------------|--------------------------|
| 2004-07-18 | 2005-01-01               |
| 2005-04-10 | 2006-01-01               |
| 2003-05-01 | 2004-01-01               |

</div>

`TRUNC`: returns a DATE truncated to the unit specified by the second argument.

```sql
SELECT hire_date,
TRUNC(hire_date, 'Month')
FROM employees
WHERE department_id = 50;
```

<div style="margin: auto; width: 100%">

| HIRE_DATE  | ROUND(HIRE_DATE, 'Month') |
|------------|---------------------------|
| 2004-07-18 | 2004-07-01                |
| 2005-04-10 | 2005-04-01                |
| 2003-05-01 | 2003-05-01                |

</div>

Another Example

```sql
SELECT hire_date,
TRUNC(hire_date, 'Year')
FROM employees
WHERE department_id = 50;
```

<div style="margin: auto; width: 100%">

| HIRE_DATE  | ROUND(HIRE_DATE, 'Year') |
|------------|--------------------------|
| 2004-07-18 | 2004-01-01               |
| 2005-04-10 | 2005-01-01               |
| 2003-05-01 | 2003-01-01               |

</div>

Here is an example of a query using multiple date functions.

```sql
SELECT EMPLOYEE_ID , hire_date, 
ROUND(MONTHS_BETWEEN(SYSDATE, hire_date)) as TENURE,
ADD_MONTHS (hire_date, 6) AS REVIEW,
NEXT_DAY(hire_date, 'FRIDAY'), LAST_DAY(hire_date)
FROM EMPLOYEES e 
WHERE MONTHS_BETWEEN (SYSDATE, hire_date) > 36
```

![date-functions-result](../../assets/img/date-functions-result.PNG)

## 10. Terminology

* Key terms used in this lesson included:
  * ADD_MONTHS
  * LAST_DAY
  * MONTHS_BETWEEN
  * NEXT_DAY
  * SYSDATE
  * ROUND
  * TRUNC

## 11. Summary

In this chapter you should have learned how to:

* Select and apply the single-row functions.
* Explain how date functions transform Oracle dates into date data or numeric values
* Demonstrate proper use of the arithmetic operators with dates.
* Demonstrate the use of `SYSDATE` and date functions
* State the implications of world businesses to be able to easily manipulate data stored in date format.


