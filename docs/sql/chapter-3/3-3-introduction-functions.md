# 3.3 Introduction to functions

## 1. Objectives

* This chapter covers the following objectives:
  * Identify appropriate applications of single-row functions in query statements
  * Classify a function as a single-row or multi-row function.
  * Differentiate between single-row functions and multiple-row functions and the results returned by each.

## 2. Purpose

When you put money in a drink machine, something happens between the time the money is deposited and your favorite drink is 
dispensed. The transaction is processed internally by the machine. Your money is the input and the drink
is the output. The machine performs a function.

* The machine:
  * Counts your money
  * Makes sure your selection is chosen
  * Returns change, if necessary.

In SQL, there are many types of functions that are used to transform input in one form to output in another form.
These functions are used to manipulate data values. Functions are small programs
that perform an action on a value or column and produce something different as output.

## 3. Functions

* Functions are both input and output. Input into a function is referred to as an argument.

<div align="center">

![function](../../assets/img/function.PNG)

</div>

> In the drink machine example, the input is money and the output is a drink.

* Oracle has two distinct types of functions:
  * `Single-row`
  * `Mutiple-row`

<div align="center">

![type-function](../../assets/img/type-function.PNG)

</div>

## 4. Single-row VS Mutiple-row functions

``Single-row`` functions operate on single rows only and return one result per row. There are different types of
single-row functions including, character, number, date and conversion functions

--------->SINGLE-ROW FUNCTION-------->

`Mutiple-row` functions can manipulate groups of rows to give one result per group of rows. There functions
are also known as group functions.

---------><br/>
--------->MULTIPLE-ROW FUNCTION---------><br/>
--------->

## 5. Single-row functions

* In SQL, Single-row functions can be used to:
  * Perform calculations such as rounding numbers to a specified decimal place.
  * Modify individual data items such as converting character values from uppercase to lowercase.
  * 
<div align="center">

![single-row-func](../../assets/img/single-row-func.PNG)

</div>

* Single row functions accept one or more arguments and will return a single result per row. So if you apply
the single row function to 12 rows, you will get 12 results out of the single row function. In summary, single-row
functions do the following:
  * Manipulate data items
  * Accept arguments and return one value
  * Act one each result per row
  * Can modify the data type
  * Can be nested

## 6. Multiple-row functions

* `Mutiple-row` (or group) functions take many rows as input and return a single value as output. The rows
input may be the whole table or the table split into smaller groups. Example of multiple-row (group)
functions include:
  * `MAX`: finds the highest value in a group of rows.
  * `MIN`: finds the lowes value in a group of rows.
  * `AVG`: finds the average value in a group of rows.

## 7. Terminology

* Key terms used in this chapter included:
  * Single Row function
  * Multiple row function

## 8. Summary

* In this chapter, you should have learned how to:
  * Identify appropriate applications if single-row functions in query statements.
  * Classify a function as a single-row or multi-row function.
  * Differentiate between single-row functions and multi-row functions and the results returned by each.
