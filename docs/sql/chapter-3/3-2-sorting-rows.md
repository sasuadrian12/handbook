# 3.2 Sorting Rows

## 1. Objects

* This chapter covers the following objects:
  * Construct a query to sort a result set in ascending or descending order.
  * State the order in witch expressions are evaluated and calculated based on the rules of precedence.
  * Construct a query to order a result set using a column alias.
  * Construct a query to order a result set for single or multiple columns.
  
## 2. Purpose

* By nature, most if us need order in our lives. Imagine if each time you had dinner, you had to look in
every kitchen drawer or cabinet to find a knife and a fork? `Ordering, group and sorting`
makes finding things easier.

> Biologist group animals in phyla, astronomers order brightness of stars by magnitude, 
and Java programmers organize code in classes.

* Or everyday lives are ordered in many situations:
  * **Library books in library**
  * **Grocery-store shelves**
  * **Documents stored in file cabinets**

* Being able to sort results is a convenient feature in SQL and enables programmers to display
information in many different ways. For database design, business functions are ordered by entities
and attributes; in database information, SQL uses the `ORDER BY` clause.

## 3. Order by clause

Information sorted in ascending order is familiar to most of us. It's what makes looking up a number in a phone book,
finding a word in the dictionary or locating a house by its street address relatively easy.

SQL use the `ORDER BY` clause can specify several ways in witch to order rows returned in a query.

!> - The default sort order is ascending. <br/> - Numeric values are displayed lowest to highest. <br/> - Date values are 
displayed with the earliest value first. <br/> - Character values are displayed in alphabetical order. <br/> - `Null` 
values are displayed in ascending order and first in descending order. <br/> - `NULLS FIRST` Specified that NULL values
should be returned before non-NULL values. <br/> - `NULLS LAST` Specified that null values should be 
returned after non-NULL values.

* The following employees example use the `ORDER BY` clause to order **hire_date** in ascending (default) order.

?> Note: The `ORDER BY` clause must be the last clause of the SQL statement.

```sql
SELECT last_name, hire_date
FROM employees
ORDER BY hire_date;
```

<div style="margin: auto; width: 50%">

| LAST_NAME | DATE         |
|-----------|--------------|
| King      | 17-Jun-1987  |
| Whalen    | 17-Sep-1987  |
| Kochhar   | 21-Sep-1989  |
| Hunold    | 03-Jan- 1990 |

</div>

## 4. Sorting in Descending Order

You can reverse the default order in the order by clause to descending order by specifying the `DESC` keyword after the column name in the order by clause.

```sql
SELECT last_name, hire_date
FROM employees
ORDER BY hire_date DESC;
```
## 5. Using Column Aliases

You can order data by using a column alias. The alias used in the `SELECT` statement is referenced in the `ORDER BY` clause.

```sql
SELECT last_name, hire_date
AS "Date Started"
FROM employees 
ORDER BY "Date Started";

```

<div style="margin: auto; width: 50%">

| LAST_NAME | Date Started |
|-----------|--------------|
| King      | 17-Jun-1987  |
| Whalen    | 17-Sep-1987  |
| Kochhar   | 21-Sep-1989  |
| Hunold    | 03-Jan- 1990 |
| Ernst     | 21-May-1991  |

</div>

## 6. Sorting with Other Columns

* It also possible to use the `ORDER BY` clause to order output by a column that is not listed in the `SELECT` clause.
In the following example, the data is sorted by the last_name column even though this column is not listed in the `SELECT` statement.

```sql
SELECT employee_id, first_name
FROM employees
WHERE employee_id < 105 
ORDER BY last_name;
```
<div style="margin: auto; width: 50%">

| EMPLOYEE_ID | FIRST_NAME |
|-------------|------------|
| 102         | Lex        |
| 104         | Bruce      |
| 103         | Alexander  |
| 100         | Steven     |
| 101         | Neena      |

</div>

## 7. Order of Execution

* The order of execution of a `SELECT` statement is as follows:
  * `FROM` clause: locates the table that contains the data.
  * `WHERE` clause: restricts the rows to be returned.
  * `SELECT` clause: selects from the reduced data set the columns requested.
  * `ORDER BY` clause: orders the result set.
  
## 8. Sorting with Multiple Columns

* It is also possible to sort query results by more than one column. In fact, there is no limit on how many column you can add to the `ORDER BY` clause.
An example of sorting with multiple columns is shown below. Employees are first ordered by department number (from lowest to highest),
* then for each department, the last names are displayed in alphabetical order (A to Z).

```sql
SELECT department_id, last_name 
FROM employees 
WHERE department_id <=50
ORDER BY department_id,
last_name
```
<div style="margin: auto; width: 50%">

| DEPARTMENT_ID | LAST_NAME |
|---------------|-----------|
| 10            | Whalen    |
| 20            | Fay       |
| 20            | Hartstein |
| 50            | Davies    |

</div>

To create an `ORDER BY` clause to sort by multiple column, specify the columns to be returned and separate the column
names using commas. If you want to revert the sort order of a column, add `DESC` after its name.

```sql
SELECT department_id, last_name
FROM employees 
WHERE department_id <= 50
ORDER BY department_id DESC,
last_name;
```
<div style="margin: auto; width: 50%">

| DEPARTMENT_ID | LAST_NAME |
|---------------|-----------|
| 50            | Davies    |
| 50            | Matos     |
| 50            | Mourgos   |
| 20            | Fay       |

</div>

## 9. Terminology

* Key terms used in this chapter included:
  * ORDER BY clause
  * ASCENDING
  * DESCENDING
  * Order of Execution.

## 10. Summary

* In this chapter, you should have learned how to:
  * Construct a query to sort a result set in ascending or descending order.
  * Construct a query to order a result set using a column alias.
  * Construct a query to order a result set for single or multiple columns.
