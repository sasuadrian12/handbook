# 3.1 Logical Comparisons and Precedence Rules

## 1. Objectives

* This chapter covers the following objectives:
  * Evaluate logical comparisons to restrict the rows returned based on two or more conditions.

  * Apply the rules of precedence to determinate the order in which expressions are evaluated and calculated.

## 4. Purpose

* Not too many things in life depend on just one condition. For instance, if you want to go to college, you probable need good grades and the tuition money to pay fot it. If you have extra money, you could either save it or spend it. If you want to go to a movie, you may want to go this weekend and you may not want to sit in the first 10 rows of the theater.

* In SQL, it is often desirable to be able to restrict the rows returned by a query based on two or mode conditions. As the manager of a fast-food business, you may need to know the names of your staff who are either cooks or oder takers. You don't need or want the entire staff list, you just want a subset of it. 

* Conditional operators such as `AND`, `NOT` and `OR` make these types of requests easy to do.

## 5. Logical Conditions

* Logical conditions combine the result of two component conditions to produce a single result bases on the,. For example, to attend a rock concert, you need to buy a ticket `AND` have transportation to get there. If both conditions are met, you can go to the concert. What if you can't get transportation, can you go?

* Another logical condition combines two component conditions with `OR`. All employees will receive a raise either by having a perfect attendance record `OR` by meeting their monthly sales quota. If an employee meets either of these two conditions, the employee gets a raise.

* A logical operator combines the results of two or more conditions to produce a single result. A result is returned `IF ONLY` the overall result of the condition is true.
  * `AND` -- return TRUE if both conditions are true.
  * `OR` -- returns TRUE if either condition is true
  * `NOT` -- returns true of the condition is false.

## 6. AND Operator

* In the query below, the results returned will be rows that satisfy BOTH conditions specified in the `WHERE` clause.

```sql
SELECT last_name, department_id, salary
FROM employees
WHERE department_id > 50 AND salary > 12000;
```

<div style="margin: auto; width: 50%">

| LAST_NAME  | DEPARTMENT_ID | SALARY |
| ----------- | ----------- | ----------- |
| King      | 90 | 24000 |
| Kochhar      | 90 | 17000 |
| De Haan      | 90 | 17000 |

</div>

* Another example of using `AND` in there where clause:

```sql
SELECT last_name, hire_date, job_id
FROM employees
WHERE hire_date > '01-Jan-1998' AND job_id LIKE 'SA%';
```

<div style="margin: auto; width: 50%">

| LAST_NAME  | DEPARTMENT_ID | SALARY |
| ----------- | ----------- | ----------- |
| Zlotkey      | 29-Jan-2000 | SA_MAN |
| Kochhar      | 24-Mar-1998 | SA_REP |
| De Haan      | 24-May-1999 | SA_REP |

</div>

* If there `WHERE` clause use the `OR` condition, the results returned from a query will be rows that satisfy wither one of the `OR` conditions.

* In other words, all rows returned have a location_id of 2500 `OR` they have a manager_id equal to 124.

```sql
SELECT department_name, manager_id, location_id
FROM departments
WHERE location_id = 2500 OR manager_id = 124;
```

<div style="margin: auto; width: 50%">

| DEPARTMENT_NAME  | MANAGER_ID | LOCATION_ID |
| ----------- | ----------- | ----------- |
| Shipping      | 124 | 1500 |
| Sales      | 149 | 2500 |

</div>

## 7. NOT Operator

- The `NOT` operator will return rows that do `NOT` satisfy the condition in the `WHERE` clause.

```sql
SELECT department_name, location_id
FROM departments
WHERE location_id NOT IT (1700, 1800);
```

<div style="margin: auto; width: 50%">

| DEPARTMENT_NAME  | LOCATION_ID |
| ----------- | ----------- |
| Shipping    | 1500 |
| IT       | 1400 |
| Sales       | 2500 |

</div>

## 8. Rules of precedence or What Happens First?

- Notice that the `AND` operator is evaluated before the `OR` operator. This means that for the example on the previous slide, if wither of the conditions in the `AND` statement are not met, then the `OR` operator is used to select the rows. This is an important concept to remember.

- First, the `AND` condition is evaluated, so all employees working in dept 80 or 50 `AND` who have a fist name starting with "C" are returned. 

- The `OR` clause is then evaluated and returns employees whose last name contains "s"

```sql
SELECT last_name ||' '|| salary * 1.05
AS "Employee Raise", department_id, first_name
FROM employees 
WHERE department_id IN(50,80)
AND first_name LIKE 'C%'
OR last_name like '%s%';
```


<div style="margin: auto; width: 50%">

| Employee Raise | DEPARTMENT_ID | FIRST_NAME |
| ----------- | ----------- | ----------- |
| Higgins 12600     | 110 | Shelley |
| Mourgos  6090    | 50 | Kevin |
| Rajs  3675    | 50 | Trenna |
| Davies  3255    | 50 | Curtis |
| Matos  2730    | 50 | Randall |
| Vargas  2625    | 50 | Peter |
| Ernst  6300    | 60 | Bruce |
| Hartstein  13650    | 20 | Michael |

</div>

* In this example, the order of the `OR` and `AND` have been reversed from the previous slide.

?> The order of operations is: first_name starts with a "C" `AND` last_name contains an "s". Both these conditions must be met to be returned. Any instance of employees in department 50 and 80 will be returned.

* Adding parenthesis changes the way the `WHERE` clause is evaluated and the rows returned.

?> The order of operations is: The values in the parentheses are selected. All instances of the values in the parentheses that also contain the letter "s" in their last_name will be returned.

## 9. Terminology

* Key terms used in this lesson included:
  * AND
  * OR
  * NOT
  * Precedence Rules

## 10. Summary

* In this chapter, you should have learned how to:
  * Evaluate logical comparisons to restrict the rows returned based on two or more conditions.
  * Apply the rules of precedence to determine the order in which expressions are evaluated and calculated.