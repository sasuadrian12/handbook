# Termeni, principii, metode

1. Termeni de baza si notiuni fundamentale din domeniul HCL
2. Principalii factori in HCL (**O**mul, **M**asina si **I**nteractiunea)
3. Componentele Framework-ului (**U, S, I, O**)
4. Metode de descrierea a interactiunii om-calculator
5. Domeniile inrudite;
6. Interaction Design (**ID**)
7. Interfata cu utilizatorul (**UI**)
8. User Experience (**UX**)