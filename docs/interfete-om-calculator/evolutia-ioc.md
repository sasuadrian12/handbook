# Evolutia IOC

La inceputul erei calculatoarelor (anii '50), utilizatorii (in special oamenii de stiinta) aveau nevoie sa devina specialisti in calculatoare pentru a le putea utiliza.

Odata cu introducerea <em>mini-</em> si a <em>microcalculatoarelor</em>(anii '70), mase mai largi de utilizatori au acces la tehnilogia informatica si <span style="color: #d96363"> utilizarea</span> calculatoarelor si a programelor incepe sa devina o problema (Shackel 1990).

Accesul pus se deplaseaza de la <span style="color: #4b46d2">aspectele fizice</span> ale interactiunii (pozitia ecranului si a tastaturii, culori, luminozitate etc) catre <span style="color: #4b46d2"> aspectele mentale</span>, acestea constind centrul preocuparilor de <span style="color: #d96363">ergonomie cognitiva</span> in mod diferentiat.

Apoi accentul s-a deplasat treptat de la <span style="color: #4b46d2"> procesele cognitive periferice</span> (<span style="color: #d96363"><em>atentia, perceptia, incarcarea informationala</em></span>) catre <span style="color: #4b46d2">procesele cognitive centrare</span> (<em> memoria, rationamentul, rezolvarea de probleme</em>) si mai nou catre procesele noncognitive (<span style="color: #d96363">motivatie, afectivitate</span>).

> :bulb: Interactiounea om calculator (IOC) este disciplina care se ocupa cu crearea, evaluarea si implementarea de sisteme de calculatoare interactive pentru utilizare umana si cu studiul fenomenelor majore adiacente.

Omul interactioneaza direct cu hardware-ul de <em>INPUT</em> si de <em>OUTPUTM</em>. Pe hardware-ul de OUTPUT, de exemplu monitor, se afla interfata grafica, produsa de software.


![linux_kernel_ubiquity](../assets/img/linux_kernel_ubiquity.png)

# Interactiunea Om-Calculator

Interactiunea om calculator (IOC), este interactiunea dintre oamenii (utilizatorii) si calculatoare, sau si activitatea de cercetare a acesteia. Este un domeniu interdisciplinar, care leaga informatica de alte domenii de studiu si cercetare. Interactiunea dintre utilizatori si calculatoare apare la nivelul Interfetei cu utilizatorul (UI) [ interface ], care include aspecte ergonimice, software si hardware, de exemplu la perifeticele calculatoarelor de uz general sau si la sisteme mecanice mari precum avioane, aeropoarte, uzine electrice, retele de elergie sau si informatice si multe altele. 

# Interfete de hardware
- Pe un monitor sau ecran de calculator. Engleza: <em>Graphic User Interface sau GUI</em>, mijlocita prin tastatura si un dispozitiv de indicat cum este mouse-ul (<span>Interfata grafica</span>).
- Mijlocita prin atingerea monitorului (tehnologia <span><em>touch screen</em></span>).
- Mijlocita prin comenzi verbale (<span><em>recunoasterea vorbirii</em></span>).

# Interfete de software

- <em>Interfata grafica</em>
- <em>Interfata text</em>

> :exclamation: <span style="color: #d96363">CHI</span> - Computer-Human-Interaction <br>
> <span style="color: #d96363">HCI</span> - Human Computer Interaction <br>
> <span style="color: #d96363">MMI</span> - Man Machine Interaction.

:thumbsup: Tot mai multe produse si sisteme <span style="color: #63d9ac"><em>vor deveni de complexitate mare</em></span>, ceia ce implica provocari pentru utilizatori. <br>
Multe produse ar trebui sa fie proiectate <span style="color: #63d9ac">pentru a se acomoda utilizatorilor lor si nu invers</span>. <br> Produsele de baza procesor pot fi extrem de versatile, dar interactiunea lor cu utilizatorul poate fi de asemenea adaptata relativ usor. 

<div align="center">

![error-library](../assets/img/error-library.PNG)

</div>
Fie imaginea pe care o vedem aproape tot timpul.

- Ce este bine cu design-ul acestei casete de eroare? <br> <em>Utilizatorul stie ca este o eroare.</em>
- Ce este proiectat prost in ceea ce priveste design-ul acestei casute de eroare? <br> <em>Descurajator <br> Nu sunt suficiente informatii <br> Nicio modalitate de a rezolva problema (instructiuni sau informatii de contact si de rezolvare)</em>.

# De ce ne intereseaza studiul HCI?

1. <span style="color: #63bad9">D.p.d.v Social</span>. Calculatoarele fac parte din societatea noastra si nu poate fi ignorat:
- <em>Ne educa copii.</em>
- <em>Furnizeaza sfaturi medicale.</em>
- <em>Gestioneaza siguranta cartilor de credit.</em>
- <em>Controleaza traficul aerian si rutier.</em>
- <em>Controleaza centrale chimice/nucleare.</em>
- <em>Controleaza fluxul de trafic (misiuni) spatiale/aeriene.</em>
- <em>Asista oamenii in sarcini de zi cu zi (aplicatii de birou).</em>
- <em>Controleaza echipamente complexe: autoturisme/masini de spalat/dispozitive climatice.</em>
- <em>Ofera modalitati de divertisment (jocuri, stimulare intelectuala etc).</em>

2. <span style="color: #63bad9">D.p.d.v al utilizatorului (omului)</span>:
- Oamenii vad calculatoare ca **aparate / echipamente** si dorect sa fie cat mai performate.
- Au limitari de memorie, procesare, perceptie.
- <span style="color: #d96363">Erorile sistemelor sunt costisitoare si determina: <em>pierderi de timp, bani, vieti omenesti</em></span>
- O proiectare buna poate sa reduca aceste limitari.
- Studiul interfetei cu informatii.
- Raspundem la intrebari <span style="color: #46d2a4">"<em>cat de mare ar trebui sa fac butoanele</em>"</span> sau <span style="color: #46d2a4">"<em>cum sa organizez optiunile de meniu</em>"</span>.
- Poate afecta:
    - Efacicatea
    - Productivitatea
    - Moralul
    - Siguranta
- O mare parte a muncii este pentru programele "reale": aproximativ <span style="color: #63bad9">50%</span>
- Software-ul "real" este destinat altor persoane decat dumneavoastra.
- Interfata utilizator proasta costa
    - Bani (satisfactie de <span style="color: #63bad9">5%</span> -> pana la <span style="color: #63bad9">85%<span> profit)
    - Vieti
- Interfetele utilizatorilor (UI) sunt greu de obtinut

# Cu ce se ocupa HCI?

Interfata si Interactiunea dintre om si calculator trebuie sa permita indeplinirea tuturor sarcinilor si obiectivelor utilizatorului.
- <span style="color: #46d2a4">Interfata</span> trebuie sa fie usor de utilizat (consistenta din punct de vedere ergonomic) si consistenta cu asteptarile utilizatorului.
- <span style="color: #46d2a4">Interactiunea</span> trebuie sa fie cat se poate de simpla si sa suporte secventa naturala de actiuni pe care o realizeaza un utilizator la relizarea unei anumite functii; pentru acesta trebuie analizate scenarii de folosire din documentul de specificatie a cerintelor.
- De ce apar probleme de utilizabilitate?
    - Interactiunea - parteneri/limbaj/proiectare
    - Cine proiecteaza interactiunea?