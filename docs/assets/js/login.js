const inputs = document.querySelectorAll(".input");

function addcl () {
  let parent = this.parentNode.parentNode;
  parent.classList.add("focus");
};

function remcl () {
  let parent = this.parentNode.parentNode;
  if (this.value == "") {
    parent.classList.remove("focus");
  }
};

inputs.forEach((input) => {
  input.addEventListener("focus", addcl);
  input.addEventListener("blur", remcl);
});

const validateForm = () => {
  //Login
  let email = document.getElementById("username").value;
  let password = document.getElementById("password").value;
  const data = { email: `${email}`, password: `${password}` };

  fetch("https://sasuke-sasuadrian12.vercel.app/api/auth/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then(function (response) {
      if (response.ok) {
        alert("Login successfully");
        window.location.replace("/#"); // Redirecting to other page.
        return response.json();
      } else {
      }
      return Promise.reject(response);
    })
    .then(function (data) {
      // This is the JSON from our response
      sessionStorage.setItem("authToken", data.token);
      console.log(data);
    })
    .catch(function (err) {
      // There was an error
      alert("Wrong password");

      sessionStorage.removeItem("authToken");
      console.warn("Something went wrong.", err);
    });
};
