# Tipul abstract de date (Abstract Data Type – ADT)


- Primul lucru pe care il facem cand scriem un program care sa ne usureze munca este sa gasim un model ce simplifica realitatea, prin separarea detaliilor care intereseaza, de cele care nu afecteaza problema pe care o rezolvam. Datele cu care se lucreaza, operatiile care se executa tin asadar de specificul fiecarei probleme tratate <br/><br/>
- Spre exemplu, pentru un program de calculul automat al cotelor de intretinere, sunt importante anumite atribute ale unei persoane (nume, salariu, numar de persoane, aflate in intretinere, suprafata locuibila pe care o detine etc), in timp ce pentru calculul salariului primit de o persoana sunt necesare informatii precum: nume, categoria de incadrare, vechimea etc. Acest proces de grupare a datelor si metodelor de prelucrare specifice rezolvarii unei probleme se mai numeste **abstractizare**. <br/> <br/>
- Putem considera deci **tipul abstract Pers** continand **atribute** si **metode** specifice, asa cum vorbim despre tipul _int_ sau _float_.
- Primul pas in aceasta grupare l-au reprezentat structurile; ele permiteau declararea unor ansambluri eterogene de date ce erau manipulate unitar. Aplicarea operatorului de _typedef_ unei structuri introducea practic un nou tip de data, construit de utilizator, tip ce putea fi aplicat unei variabile la alocarea si initializarea ei:

```c++
typedef struct 
    {
        char name[20];
        int ege;
        floar salary;
    }Pers;
Pers p1 = {"Mai Treyi", 25, 85000.}, P2, *pp;
```

- **Includerea in structura a unor pointeri** catre alte variabile sau zone dinamice continand date de acelasi tip sau catre diverse alte entitati, a permis relativizarea controlului actiunii la un context dat. Era, spre exemplu, mult mai greu sa se gestioneze la nivel central toate meniurile si submeniurile cu care lucra un program, dar s-a dovedit a fi relativ simplu, daca organizam meniul ca o structura, iar in fiecare structura punem si pointeri care sa ne indice unde ne putem îndrepta (stanga, dreapta, sus, jos) sau ce putem executa, cand am atins un context oarecare, selectand o optiune.

## 1.1 Concepte de clasa si obiecte

### A. Clasa si obiecte

Daca intr-un astfel de tip de data **introducem chiar si functii** (gestionate flexibil prin pointeri) care sa ne spuna la ce prelucrari specifice sunt supuse datele structurii, obtinem asa numitul **tip de utilizator** (_user built-in_) suplinit in limbajul C++ prin notiunea de **clasa**. Ea implementeaza un concept abstract, indicand _natura_ **datelor** ce-l compun, precum si **metodele** (functii si operatorii specifici) ce-i pot fi aplicate.
<br /> <br />

* Din punct de vedere sintactic, o clasa in C++ se defineste sub forma:

```c++
class nume_c
{
    //date + functii membre
}
```

?> Unde: <br/> - nume_c reprezinta numele dat de programator clasei respective. <br/> _Avantajele unei asemenea abordari sunt imense:_ <br/> - specializarea prelucrarilor si adaptarea lor de la un caz la altul; <br /> - localizarea facila a erorilor; <br/> - surprinderea intr-un mod specific a tipologiei relatiilor dintre entitati; <br/> - gestionarea accesului prin "ascunderea" unor data, restrictionarea folosirii unor functii, obtinerea unor informatii doar prin functii de acces specializare.

Vom incerca sa exemplificam conceptele propuse, pe obiecte concrete ale lumii reale, amintind o data in plus, ca proiectarea si programarea orientate obiect au valoare doar atunci cand structura interna a obiectului si interfata acestuia cu celelalte obiecte isi gasesc o **reflectare in lumea reala**, altfel raman doar o teorie frumoasa. Tocmai aceste legaturi, pe care utilizatorul le cunoaste deja, faciliteaza intelegerea programelor, manipularea si dezvoltarea lor ulterioara. <br/> <br/>

Functii si datele unei clase pot fi grupate, din punct de vedere al dreptului de acces, in trei categorii, demarcate prin etichete cheie **_private, public_** si **_protected_** si care surprind drepturile de acces ale unei terț la respectivele resurse ale structurii: <br/>

```c++
class Pers
{
    private: int age;
    protected: float salary;
    public:
        char name[20];
        void int(char * n = "Anonim", int v=0, floar s=0.)
            { strcpy(name, n);
              age = v;
              salary = s;
            }
            char * call_name();
            int call_age() { return age; }
}
char * Pers::call_name()
    {
        return nume;
    }
```

* Din definirea clasei _Pers_ se pot observa cateva aspecte:
  * datele membre se declara obisnuit, ca si in cazul structurilor;
  * functiile membre se pot defini direct in casa fara a necesita o sintaxa speciala si sunt functii _inline_ (in cazul nostru _init()_ si **call_age()**) sau se pot doar declara in clasa (li se scrie doar prototipul) si **se definesc in afara ei** folosindu-se sintaxa:

```c++
type name_c::name_m (list_p_f) {...........}
```

?>Unde: <br/> - _type_ - reprezinta tipul functiei membre; <br/> - **name_c** - este numele clasei; <br/> - **name_m** - este numele metodei sau al functiei membre; <br/> - **list_p_f** - reprezinta lista parametrilor formali; <br/>

!> Cum este cazul metodei **call_name()**

Odata definit acest tip, el poate fi folosit la declarerea unor variabile de acest gen (fara a mai folosi cuvantul cheie _class_). Ca si la structuri, daca **numele de clasa lipseste**, declaratiile de variabile se pot face numai odata cu definirea clasei, deoarece nu mai dispunem de nume de sablon pentru descriere:

```c++
class
{
    // date si functii membre
} v1,v[5], *pv;
```

Indiferent ca se declara sau nu variabile, <span style="color: yellow">semnul punct si virgula << ; >></span> din finalul declaratiei,  <span style="color: yellow">este obligatoriu</span>, lipsa lui fiind poate cea mai raspandita eroare de sintaxa, antrenand neintelegeri majore pentru compilator. <br/>

O variabila de tip clasa poate stoca un set complet al datelor clasei respective. Acest set concret de date reprezinta o instantierea clasei, adica o manifestare concreta a clasei. In teoria POO, o astfel de instanta poarta numele de **obiect**. <br/>

Referitor la clasa **Pers** definita mai sus, din programul urmator:

```c++
#include <iostream>
using namespace std;

void main()
{
    Pers p;
    p.init(); //we initializate with implicite values
    count << p.call_name();
}
```

?> Unde: <br/> - _p_ - este in obiect de tip Pers; <br/> - apelul unei metode se face in legatura cu un obiect concret, forma generala fiind: _object.metoda(parametri_de_apel)_; <br/> - expresia **count << p.call_name();** are drept consecinta afisarea pe monitor a variabilei nume. <br/>

Se impune in acest moment a face o scurta paranteza cu privire la modul de realizare a operatiilor de intrare/iesire pe dispozitivul standard (tastatura/monitor). Aceste operatii le vom face folosind doua obiecte predefinite (**cin** si **cout**) de clasa _istream_, respectiv _ostream_, specializate in acest sens. <br/>

Afisarea pe monitor se face construind o expresie de genul: _cout << var_c;_ unde, _var_c_ este o variabila sau o constanta. Intr-un literal secventele de **escape** sunt recunoscute, efectul fiind identic ca si in cazul folosirii lor in functia _printf()_. <br/>

De exmeplu secventa:

```c++
int a = 17;
cout << a;
cout<< "\n a="<<a;
```
?> Will be:

```c++
17
a = 17
```
Se observa ca putem afisa mai multe date intr-o singura expresie, doar ca ele trebuie transmise operatorului **<<**. In constanta literal secventa '\n' determina trecerea la linie noua. <br/> Pentru citirea de la tastatura a unei variabile se foloseste obiectul **cin** in forma: _cin>>var;_ unde, var este numele unei variabile. De exemplu secventa;

```c++
int a;
cin >> a;
```

Determina citerea variabilei _a_ de tip intreg; daca se doreste a se citi intr-o singura expresie mai multe variabile, ele se vor inlantui cu ajutorul operatorului _>>_:

```c++
int a;
double t;
cin>>a>>t;
```
Folisirea obiectelor _cin_ si _cout_ implica includerea fisierului header si a namespace-ului care defineste clasele si metodele invocate.

```c++
#include <iostream>
using namespace std;
```

* Mai multe detalii despre realizarea operatiilor de intrare/iesire folosind aceste obiecte gasiti in capitolul _Operatii de intrare/iesire orientate pe stream-uri_. Revenind la exemplul nostru, putem concluziona ca un membru al clasei este calificat:
  * prin **operatorul de rezolutie ::** in raport cu clasa careia apartine;
  * cu ' . ' sau ' -> ', in raport cu instantierile clasei (obiect, resprectiv pointer la obiect). <br/>

Astfel, la definire, functia _spune_nume()_ poarta in fata calicativul **Pers::**, in timp ce la apel se particularizează obiectivul pentru care lucraza: **p1**.call_name(). <br/>

Domeniul **public** cuprinde datele si functiile membre ce sunt accesate prin intermediul obiectului si pot fi folosite sau apelate de oricare alte functii din cadrul programului. DOmeniul **private** cuprinde datele si functiile membre ce pot fi folosite doar de catre celelalte functii apartinand clasei respective. <br/>

In afara celor doua domenii, in C++ poate fi delimitata si o zina denumita **protecterd**, similara cu _private_, dar care da totusi drepturi de acces functiilor membre ale claselor derivate (dezvoltate) din clasa respectiva. Asadar, _protected_ este un atribut mai slab decat private, dar mai restrictiv decat public. <br/>

* Fata de **teoria POO**, in C++, lucrurile sunt oarecum simplificate; doua sunt deosebirile majore intre cele doua abordari:
  * In teoria POO nu se admit date de domeniu _public_, toate fiind _private_;
  * Obiectele sunt active (primesc in permanenta si trateaza mesajele), adica cel putin o functie de interfata este activa in permanenta, pentru a sesiza mesajele adresate obiectului, implementarea fiind deci una de timp multitasking. Din aceasta cauza, conceptul de clasa din C++ mai este denumit prin **ADT** (Abstract Data Type) pentru a-l distinge de clasa, in acceptiunea teoriei POO.

Eticheta **private** poate lipsi, subintelefandu-se ca private resursele ce nu apar in domeniul public. Cand nu apare nici eticheta **public**, intreaga clasa este deci privata, accesul asigurandu-se in exclusivitate prin interfata (functiile de acces mentionate de clasa). Prin contrast cu clasele, din punct de vedere al accesului, struncturile se considera entitati cu acces implicit public. In C++ domeniile de acces pot fi mentionate explicit si pentru structuri, struct devenind un echivalent aproape perfect al lui _class_. <br/>

Cand sunt mentionate explicit toate domeniile de acces, ele se dau uzual in ordinea **private, public, protected**, dar practic pot aparea in orice succesiune, putand-se reveni de mai multe ori asupra unui domeniu:

```c++
class Pers
{
  private: int age;
  public: char name[20];
  protected:
    void init (char *n="Anonim", int v = 0, float s = 0.) 
          { 
          strcpy (name, n);
          age = v;
          salary = s;
          }
  public: int call_age() { return age; }
  private: float salary;
}
```

In concluzie, putem spune ca se respecta principiul conform caruia un tip de date se distinge printr-un mod de reprezentare si prin operatiile pe care le suporta; pe langa **tipurile predefinite** (de baza sau fundamentale) se pot introduce noi **tipuri de utilizator**, folosind **struct, union** sau class, al caror mod de reprezentare si operatii recunoscute se dau la definire.

* **_Incapsularea permite_**
  * un transfer mai simplu al datelor in / din functii (transferam obiectul in ansamblu, nu elementele sale);
  * controlul accesului la datele membre;
  * scuteste utilizatorul de cunoasterea unor detalii tehnice, perceptia obiectului fiind una orientata spre client.

* Dupa rolul pe care-l joaca in cadrul clasei, functiile membre ale unei clase se impart in patru categorii:
  * **constructori**: reponsabili cu crearea obiectelor;
  * **distructor**: responsabil cu distrugerea obiectelor si eliberarea memoriei ocupate de acestea;
  * **functii de acces**: care mediaza legatura obiectului cu exteriorul;
  * **metode**: functii care introduc operatiile si prelucrarile specifice obiectului.

In realitate, un obiect contine numai datele specifice, compilatorului tinand un singur exemplar din functiile membre, pe care-l particularizeaza in momentul apelului, cand va cunoaste obiectul proprietar al functiei; aceasta modalitate de lucru se explica prin faptul ca o functie membra care acelasi cod executabil pentru toate obiectele clasei, dar executa acest cod pe datele specifice fiecarui obiect.

### B. Separarea interfetei de partea de implementare
