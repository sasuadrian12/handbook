# ABSTRACTIZAREA DATELOR. CONCEPTUL DE CLASA

- Tipul abstract de date (Abstract Data Type – ADT)
- Conceptele de clasă şi obiect
- Pointeri la obiecte. Masive de obiecte
- Clase incluse. Compunerea obiectelor
- Tipologia membrilor unei clase
- Transferul obiectelor în / din funcţii
- Pointeri de date şi funcţii membre
- Clase şi funcţii prietene. Privilegii în sistemul de acces
- Modificatorul const în contextul obiectelor