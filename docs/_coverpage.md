# Documentation

> Universitatea din Bacau - Facultatea de Stiinte -
> [Matematica Informatica](https://www.ub.ro/stiinte/)

[Interface human-pc](/interfete-om-calculator/front-page)
[SQL Oracle](/sql/_coverpage.md)
[Programarea orientata obiect in limbajul c++](/oop/_coverpage.md)
[Calendar exams](exams-calendar.html)

<!-- [Demo Sandbox](https://codesandbox.io/s/xv36w4695o) -->
