# Add-Type -Path ".\itextsharp.dll"
# $file = '.\06.02.01.05_Prezentare_Domeniu_HCI_ppt (1).pdf'
# $pdf = New-Object iTextSharp.text.pdf.pdfreader -ArgumentList $file

# Write-Host($pdf)

# for($page = 1; $page -le $pdf.NumberOfPages; $page++) {
#     $text=[iTextSharp.text.pdf.parser.PdfTextExtractor]::GetTextFromPage($pdf, $page)
#     Write-Output $text
# }


function convert-PDFtoText {
	param(
		[Parameter(Mandatory=$true)][string]$file
	)	
	Add-Type -Path ".\itextsharp.dll"
	$pdf = New-Object iTextSharp.text.pdf.pdfreader -ArgumentList $file
	for ($page = 1; $page -le $pdf.NumberOfPages; $page++){
		$text=[iTextSharp.text.pdf.parser.PdfTextExtractor]::GetTextFromPage($pdf,$page)
		Write-Output $text
	}	
	$pdf.Close()
}

$file = '.\06.02.01.05_Prezentare_Domeniu_HCI_ppt (1).pdf'

convert-PDFtoText $file